package br.com.precos.domain.place;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import org.junit.Test;
import org.mockito.Mockito;

import br.com.precos.domain.localization.City;

public class PlaceTest {

    @Test
    public void should_be_created_without_id() throws NoSuchMethodException, SecurityException {
	Place Place = new Place();
	Modifier.isProtected(Place.class.getDeclaredConstructor().getModifiers());
	assertNull(Place.getId());
    }

    @Test
    public void should_retrieve_the_id() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
	Place Place = new Place();

	Field field = Place.class.getDeclaredField("id");
	field.setAccessible(true);
	field.set(Place, Long.valueOf(20L));

	assertEquals(Long.valueOf(20), Place.getId());
    }

    @Test
    public void should_be_created_with_the_description() {
	City city = Mockito.mock(City.class);
	Place place = new Place("Supermercado", city);
	assertEquals("Supermercado", place.getDescription());
	assertEquals(city, place.getCity());
    }

    @Test
    public void should_update_the_description() {
	City city = Mockito.mock(City.class);
	Place place = new Place("Supermercado", city);
	assertEquals("Supermercado", place.getDescription());
	assertEquals(city, place.getCity());

	place.setDescription("Supermercado Dubão");
	assertEquals("Supermercado Dubão", place.getDescription());
	assertEquals(city, place.getCity());
    }

    @Test
    public void should_show_the_toString() {
	City city = Mockito.mock(City.class);
	assertEquals("Place{id=null, description=Supermercado}", new Place("Supermercado", city).toString());
    }

    @Test
    public void should_has_equals_hash_with_equals_values() {
	City city = Mockito.mock(City.class);
	Place Place = new Place("Supermercado", city);
	Place Place1 = new Place("Supermercado", city);
	assertEquals(Place.hashCode(), Place1.hashCode());
    }
    
}
