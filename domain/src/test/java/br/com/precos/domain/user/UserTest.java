package br.com.precos.domain.user;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import org.junit.Test;

import br.com.precos.domain.type.Email;

public class UserTest {

	@Test
	public void should_be_created_without_id() throws NoSuchMethodException, SecurityException {
		User User = new User();
		Modifier.isProtected(User.class.getDeclaredConstructor().getModifiers());
		assertNull(User.getId());
	}
	
	@Test
	public void should_retrieve_the_id() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		User User = new User();
		
		Field field = User.class.getDeclaredField("id");
		field.setAccessible(true);
		field.set(User, Long.valueOf(20L));
		
		assertEquals(Long.valueOf(20), User.getId());
	}
	
	@Test
	public void should_be_created_with_the_name_and_email() {
		User user = User.builder()
				.email(new Email("jose@email.com"))
				.name("José")
				.build();
		
		assertEquals("José", user.getName());
		assertEquals(new Email("jose@email.com"), user.getEmail());
	}
	
	@Test
	public void should_update_the_name() {	 
		User user = User.builder()
				.email(new Email("jose@email.com"))
				.name("José")
				.build();
		assertEquals("José", user.getName());
		
		user.setName("Marion");
		assertEquals("Marion", user.getName());
	}
	
	@Test
	public void should_show_the_toString() {
		User user = User.builder()
				.name("José")
				.email(new Email("jose@email.com"))
				.build();
		
		
		assertEquals("User{id=null, name=José, email=jose@email.com}", user.toString());
	}
	
	@Test
	public void should_has_equals_hash_with_equals_values(){
		User user = User.builder()
				.name("José")
				.email(new Email("jose@email.com"))
				.build();
		
		User user1 = User.builder()
				.name("José")
				.email(new Email("jose@email.com"))
				.build();
		assertEquals(user.hashCode(), user1.hashCode());
		
		User maria = User.builder()
				.name("Maria")
				.email(new Email("maria@email.com"))
				.build();
		
		assertNotEquals(maria.hashCode(), user.hashCode());
	}

}
