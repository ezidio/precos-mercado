package br.com.precos.domain.product;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import br.com.precos.domain.place.Place;
import br.com.precos.domain.type.barcode.EAN13;

public class ProductTest {

    private static final String VALID_BARCODE = "7896007941636";

    @Test
    public void should_be_created_without_id() throws NoSuchMethodException, SecurityException {
	Product product = new Product();
	Modifier.isProtected(Product.class.getDeclaredConstructor().getModifiers());
	assertNull(product.getId());
    }

    @Test
    public void should_retrieve_the_id() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
	Product product = new Product();

	Field field = Product.class.getDeclaredField("id");
	field.setAccessible(true);
	field.set(product, Long.valueOf(20L));

	assertEquals(Long.valueOf(20), product.getId());
    }

    @Test
    public void should_be_created_with_the_description() {
	Place place = Mockito.mock(Place.class);
	Product product = new Product("Maizena", place, EAN13.from(VALID_BARCODE));
	assertEquals("Maizena", product.getDescription());
	assertEquals(EAN13.from(VALID_BARCODE), product.getBarcode());
	
    }

    @Test
    public void should_update_the_description() {
	Place place = Mockito.mock(Place.class);
	Product product = new Product("Maizena", place, EAN13.from(VALID_BARCODE));
	assertEquals("Maizena", product.getDescription());

	product.setDescription("Maizena Duryea");
	assertEquals("Maizena Duryea", product.getDescription());
    }

    @Test
    public void should_show_the_toString() {
	Place place = Mockito.mock(Place.class);
	assertEquals("Product{id=null, description=Maizena, barcode=7896007941636}", new Product("Maizena", place, EAN13.from(VALID_BARCODE)).toString());
    }

    @Test
    public void should_has_equals_hash_with_equals_values() {
	Place place = Mockito.mock(Place.class);
	Product product = new Product("Maizena", place, EAN13.from(VALID_BARCODE));
	Product product1 = new Product("Maizena", place, EAN13.from(VALID_BARCODE));
	assertEquals(product.hashCode(), product1.hashCode());
    }
    
    @Test
    public void should_has_different_hash_with_different_descriptions() {
	Place place = Mockito.mock(Place.class);
	Product product = new Product("Maizena", place, EAN13.from(VALID_BARCODE));
	Product product1 = new Product("Sabão", place, EAN13.from(VALID_BARCODE));
	Assert.assertNotEquals(product.hashCode(), product1.hashCode());
    }
    
    @Test
    public void should_has_different_hash_with_different_barcodes() {
	Place place = Mockito.mock(Place.class);
	Product product = new Product("Maizena", place, EAN13.from("2199100003115"));
	Product product1 = new Product("Maizena", place, EAN13.from(VALID_BARCODE));
	Assert.assertNotEquals(product.hashCode(), product1.hashCode());
    }
    
    @Test
    public void should_can_be_sold_from_any_place_if_place_is_null() {
	EAN13 barcode = EAN13.from("2199100003115");
	assertTrue(new Product("Maizena", barcode).canBeSoldFrom(Mockito.mock(Place.class)));
	assertTrue(new Product("Maizena", barcode).canBeSoldFrom(Mockito.mock(Place.class)));
	assertTrue(new Product("Maizena", barcode).canBeSoldFrom(Mockito.mock(Place.class)));
	assertTrue(new Product("Maizena", barcode).canBeSoldFrom(Mockito.mock(Place.class)));
    }
    
    @Test
    public void should_can_be_sold_from_the_same_place() {
	EAN13 barcode = EAN13.from("2199100003115");
	Place place = Mockito.mock(Place.class);
	assertTrue(new Product("Maizena", place, barcode).canBeSoldFrom(place));
    }

}
