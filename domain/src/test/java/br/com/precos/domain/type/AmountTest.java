package br.com.precos.domain.type;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

import org.junit.Test;

public class AmountTest {

    @Test
    public void testCreate() {
	Amount amount = new Amount(BigDecimal.ONE);
	assertEquals(BigDecimal.ONE.setScale(Amount.SCALE), amount.toBigDecimal());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateWithNull() {
	new Amount(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateWithNegative() {
	new Amount(BigDecimal.valueOf(-1));
    }

    @Test
    public void testCreateWithZero() {
	Amount amount = new Amount(BigDecimal.ZERO);
	assertEquals(BigDecimal.ZERO.setScale(Amount.SCALE), amount.toBigDecimal());
    }

    @Test
    public void testZero() {
	assertEquals(BigDecimal.ZERO.setScale(Amount.SCALE), Amount.ZERO.toBigDecimal());
    }

    @Test
    public void testEquals() {
	Amount Amount = new Amount(BigDecimal.TEN);

	assertTrue(Amount.equals(new Amount(BigDecimal.TEN)));
	assertFalse(Amount.equals(null));
	assertFalse(Amount.equals("string nada a ver"));

	assertTrue(new Amount(BigDecimal.valueOf(2.2).setScale(20)).equals(new Amount(BigDecimal.valueOf(2.20).setScale(2))));
    }

    @Test
    public void should_have_equals_hash_to_equals_values() {
	assertEquals(Amount.valueOf(2).hashCode(), Amount.valueOf(2.0).hashCode());
	assertEquals(Amount.valueOf(1.3).hashCode(), Amount.valueOf(1.3).hashCode());
	assertEquals(Amount.valueOf(2.344).hashCode(), Amount.valueOf(2.344).hashCode());
	assertEquals(Amount.valueOf(2843984).hashCode(), Amount.valueOf(2843984).hashCode());
    }

    @Test
    public void should_equal_value_be_equals() {
	assertTrue(Amount.valueOf(2).equals(Amount.valueOf(2.0)));
	assertFalse(Amount.valueOf(2).equals(null));
	assertFalse(Amount.valueOf(2).equals("another object"));
	assertFalse(Amount.valueOf(2).equals(Amount.valueOf(4)));
    }

    @Test
    public void should_retrieve_the_string_value() {
	assertEquals("2.200", Amount.valueOf(2.2).toString());
	assertEquals("234.433", Amount.valueOf(234.433).toString());
    }

    @Test
    public void testValueOfLong() {
	Amount price = Amount.valueOf(2);
	assertEquals(new Amount(BigDecimal.valueOf(2)), price);
    }

    @Test
    public void testValueOfDouble() {
	Amount price = Amount.valueOf(2.2);
	assertEquals(new Amount(BigDecimal.valueOf(2.2)), price);
    }

}
