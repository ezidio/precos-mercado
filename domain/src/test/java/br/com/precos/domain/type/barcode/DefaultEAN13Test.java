package br.com.precos.domain.type.barcode;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class DefaultEAN13Test {

    DefaultEAN13 fosforo240 = new DefaultEAN13("7896007941636"); // FOSFORO FIAT LUX COZINHA FORTE 240UN

    @Test(expected = IllegalArgumentException.class)
    public void should_throw_exception_if_create_with_invalid_code() {
	new DefaultEAN13("7896007922252");
    }

    @Test
    public void should_toString_retrieve_the_code() {
	assertEquals("7896007941636", this.fosforo240.toString());
    }

    @Test
    public void should_retrieve_the_product_code() {
	assertEquals("7896007941636", this.fosforo240.getProductCode());
    }

    @Test(expected = IllegalStateException.class)
    public void should_throw_exception_if_get_price() {
	this.fosforo240.getPrice();
    }

}
