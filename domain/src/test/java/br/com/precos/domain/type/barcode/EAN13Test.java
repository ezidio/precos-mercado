package br.com.precos.domain.type.barcode;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class EAN13Test {

    @Test
    public void should_create_custom_instance_by_EAN13() {
	String custom_code = "2199100003115";
	assertEquals(CustomEAN13.class, EAN13.from(custom_code).getClass());
	assertEquals(new CustomEAN13(custom_code), EAN13.from(custom_code));
    }
    
    @Test
    public void should_create_default_instance_by_EAN13() {
	String default_code = "7891000528709";
	EAN13 default_ean13 = EAN13.from(default_code);
	assertEquals(DefaultEAN13.class, default_ean13.getClass());
	assertEquals(new DefaultEAN13(default_code), default_ean13);
    }
    
    @Test(expected=IllegalArgumentException.class)
    public void should_throw_exception_if_create_custom_instance_with_invalid_EAN13() {
	EAN13.from("2199100003111");
    }
    
    @Test(expected=IllegalArgumentException.class)
    public void should_throw_exception_if_create_default_instance_with_invalid_EAN13() {
	EAN13.from("7891000528703");
    }
    
    @Test(expected=IllegalArgumentException.class)
    public void should_throw_exception_if_create_with_null_code() {
	EAN13.from(null);
    }
    
}
