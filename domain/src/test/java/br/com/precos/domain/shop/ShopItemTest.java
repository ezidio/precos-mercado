package br.com.precos.domain.shop;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

import org.junit.Test;
import org.mockito.Mockito;

import br.com.precos.domain.product.Product;
import br.com.precos.domain.type.Amount;
import br.com.precos.domain.type.Money;

public class ShopItemTest {

    @Test
    public void testCreate() {

	Product product = Mockito.mock(Product.class);
	Money price = new Money(BigDecimal.valueOf(2));
	Amount amount = new Amount(BigDecimal.valueOf(2));
	ShopItem shopProduct = new ShopItem(product, price, amount);

	assertEquals(product, shopProduct.getProduct());
	assertEquals(price, shopProduct.getPrice());
	assertEquals(amount, shopProduct.getAmount());
    }

    @Test(expected = IllegalArgumentException.class)
    public void should_throw_an_exception_if_specified_a_null_product() {

	Money price = new Money(BigDecimal.valueOf(2));
	Amount amount = new Amount(BigDecimal.valueOf(2));
	new ShopItem(null, price, amount);
    }

    @Test(expected = IllegalArgumentException.class)
    public void should_throw_an_exception_if_specified_a_null_price() {

	Product product = Mockito.mock(Product.class);
	Amount amount = new Amount(BigDecimal.valueOf(2));
	new ShopItem(product, null, amount);
    }

    @Test(expected = IllegalArgumentException.class)
    public void should_throw_an_exception_if_specified_a_null_amount() {

	Product product = Mockito.mock(Product.class);
	Money price = new Money(BigDecimal.valueOf(2));
	new ShopItem(product, price, null);
    }

    @Test
    public void testCreateWithProduct() {
	Product product = Mockito.mock(Product.class);
	ShopItem shopProduct = ShopItem.createWith(product);

	assertEquals(product, shopProduct.getProduct());
	assertEquals(Money.ZERO, shopProduct.getPrice());
	assertEquals(Amount.ONE, shopProduct.getAmount());
    }

    @Test
    public void should_be_invalid_if_price_is_zero() {
	Product product = Mockito.mock(Product.class);
	ShopItem item = ShopItem.createWith(product);

	assertEquals(Money.ZERO, item.getPrice());
	assertFalse(item.isValid());
    }

    @Test
    public void should_be_valid_if_price_greather_than_zero() {
	Product product = Mockito.mock(Product.class);
	ShopItem item = ShopItem.createWith(product);
	item = item.setPrice(Money.valueOf(10));
	assertTrue(item.isValid());
    }

    @Test(expected = IllegalArgumentException.class)
    public void should_be_invalid_if_amount_is_zero() {
	Product product = Mockito.mock(Product.class);
	ShopItem.createWith(product).setAmount(Amount.ZERO);
    }

    @Test
    public void should_be_valid_if_amount_greather_than_zero() {
	Product product = Mockito.mock(Product.class);
	ShopItem item = ShopItem.createWith(product);
	item = item.setAmount(Amount.valueOf(10));
	item = item.setPrice(Money.valueOf(10));
	assertTrue(item.isValid());
    }

    @Test
    public void should_retrieve_the_total_value() {
	Product product = Mockito.mock(Product.class);
	ShopItem item;

	item = new ShopItem(product, Money.valueOf(1.4), Amount.valueOf(2));
	assertNotNull(item.getTotal());
	assertEquals(Money.valueOf(2.8), item.getTotal());

	item = new ShopItem(product, Money.valueOf(1.4), Amount.valueOf(0.0500));
	assertNotNull(item.getTotal());
	assertEquals(Money.valueOf(0.07), item.getTotal());

	item = new ShopItem(product, Money.valueOf(101.4), Amount.valueOf(0.0500));
	assertNotNull(item.getTotal());
	assertEquals(Money.valueOf(5.07), item.getTotal());

	item = new ShopItem(product, Money.valueOf(9), Amount.valueOf(0.33));
	assertNotNull(item.getTotal());
	assertEquals(Money.valueOf(2.97), item.getTotal());

	item = new ShopItem(product, Money.valueOf(42.99), Amount.valueOf(0.834));
	assertNotNull(item.getTotal());
	assertEquals(Money.valueOf(35.85), item.getTotal());

	item = new ShopItem(product, Money.valueOf(19.98), Amount.valueOf(1.064));
	assertNotNull(item.getTotal());
	assertEquals(Money.valueOf(21.26), item.getTotal());

	item = new ShopItem(product, Money.valueOf(8.98), Amount.valueOf(1.756));
	assertNotNull(item.getTotal());
	assertEquals(Money.valueOf(15.77), item.getTotal());

    }

    @Test
    public void should_update_the_value_creating_another_instance() {
	Product product = Mockito.mock(Product.class);
	Money price = new Money(BigDecimal.valueOf(1.4));
	Amount amount = new Amount(BigDecimal.valueOf(2));
	ShopItem item = new ShopItem(product, price, amount);

	ShopItem newItem = item.setPrice(Money.valueOf(2.5));

	assertTrue(newItem != item);
	assertFalse(newItem.equals(item));
	assertEquals(Money.valueOf(2.5), newItem.getPrice());
	assertEquals(Amount.valueOf(2), newItem.getAmount());
	assertEquals(product, newItem.getProduct());
    }

    @Test
    public void should_update_the_amount_creating_another_instance() {
	Product product = Mockito.mock(Product.class);
	Money price = new Money(BigDecimal.valueOf(1.4));
	Amount amount = new Amount(BigDecimal.valueOf(2));
	ShopItem item = new ShopItem(product, price, amount);

	ShopItem newItem = item.setAmount(Amount.valueOf(5));

	assertTrue(newItem != item);
	assertFalse(newItem.equals(item));
	assertEquals(Money.valueOf(1.4), newItem.getPrice());
	assertEquals(Amount.valueOf(5), newItem.getAmount());
	assertEquals(product, newItem.getProduct());
    }

    @Test
    public void should_update_the_amount_and_price_creating_another_instance() {
	Product product = Mockito.mock(Product.class);
	Money price = new Money(BigDecimal.valueOf(1.4));
	Amount amount = new Amount(BigDecimal.valueOf(2));
	ShopItem item = new ShopItem(product, price, amount);

	ShopItem newItem = item.updateValues(Money.valueOf(3.4), Amount.valueOf(5));

	assertTrue(newItem != item);
	assertFalse(newItem.equals(item));
	assertEquals(Money.valueOf(3.4), newItem.getPrice());
	assertEquals(Amount.valueOf(5), newItem.getAmount());
	assertEquals(product, newItem.getProduct());
    }

    @Test
    public void testToString() {
	Product product = Mockito.mock(Product.class);
	Mockito.when(product.toString()).thenReturn("Maizena");

	Money price = new Money(BigDecimal.valueOf(1.4));
	Amount amount = new Amount(BigDecimal.valueOf(2));
	ShopItem item = new ShopItem(product, price, amount);

	assertEquals("ShopItem{product=Maizena, price=1.40, amount=2.000}", item.toString());
    }

    @Test
    public void should_equals_value_be_equals() {
	Product product = Mockito.mock(Product.class);
	Money price = new Money(BigDecimal.valueOf(1.4));
	Amount amount = new Amount(BigDecimal.valueOf(2));

	ShopItem item = new ShopItem(product, price, amount);
	ShopItem item2 = new ShopItem(product, price, amount);

	assertTrue(item.equals(item2));
	assertFalse(item.equals(null));
	assertFalse(item.equals("outro objeto qualquer"));
	assertFalse(item.equals(new ShopItem(product, price, Amount.valueOf(12))));

    }

    @Test
    public void should_hash_be_equals() {
	Product product = Mockito.mock(Product.class);
	Money price = new Money(BigDecimal.valueOf(1.4));
	Amount amount = new Amount(BigDecimal.valueOf(2));

	ShopItem item = new ShopItem(product, price, amount);
	ShopItem item2 = new ShopItem(product, price, amount);

	assertEquals(item.hashCode(), item2.hashCode());
    }

    @Test
    public void should_calculate_price_by_total_value() {

	Product product = Mockito.mock(Product.class);

	assertEquals(Money.valueOf(9), new ShopItem(product, Money.ZERO, Amount.valueOf(0.33)).setTotal(Money.valueOf(2.97)).getPrice());
	assertEquals(Money.valueOf(42.99), new ShopItem(product, Money.ZERO, Amount.valueOf(0.834)).setTotal(Money.valueOf(35.85)).getPrice());
	assertEquals(Money.valueOf(19.98), new ShopItem(product, Money.ZERO, Amount.valueOf(1.064)).setTotal(Money.valueOf(21.26)).getPrice());
	assertEquals(Money.valueOf(8.98), new ShopItem(product, Money.ZERO, Amount.valueOf(1.756)).setTotal(Money.valueOf(15.77)).getPrice());

	assertEquals(Amount.valueOf(0.33), new ShopItem(product, Money.valueOf(9), Amount.ZERO).setTotal(Money.valueOf(2.97)).getAmount());
	assertEquals(Amount.valueOf(1.064), new ShopItem(product, Money.valueOf(19.98), Amount.ZERO).setTotal(Money.valueOf(21.26)).getAmount());
	assertEquals(Amount.valueOf(1.756), new ShopItem(product, Money.valueOf(8.98), Amount.ZERO).setTotal(Money.valueOf(15.77)).getAmount());
	assertEquals(Amount.valueOf(0.834), new ShopItem(product, Money.valueOf(42.99), Amount.ZERO).setTotal(Money.valueOf(35.85)).getAmount());

    }
}
