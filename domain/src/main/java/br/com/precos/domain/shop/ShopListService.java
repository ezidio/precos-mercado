package br.com.precos.domain.shop;

import java.util.Collection;

import br.com.precos.domain.product.Product;

import com.google.common.base.Optional;

public class ShopListService {

    public Optional<ShopItem> getLastPrice(Product product, Collection<ShopList> lists) {

	ShopList found = null;

	for (ShopList shopList : lists) {

	    if (shopList.isCancelled())
		continue;
	    
	    if (!shopList.contains(product))
		continue;

	    if (found == null || shopList.getDate().isAfter(found.getDate())) {
		found = shopList;
	    }

	}

	if (found == null)
	    return Optional.absent();

	return found.getItemBy(product);
    }

}
