package br.com.precos.domain.product;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import br.com.precos.domain.place.Place;
import br.com.precos.domain.type.barcode.EAN13;

import com.google.common.base.Objects;

@Entity
public class Product {

    @Id
    private Long id;

    @Column(name = "ID_PLACE")
    @ManyToOne
    private Place place;
    
    @Column
    private String description;

    @Column
    private EAN13 barcode;

    public Product(String description, Place place, EAN13 barcode) {
	this.place = place;
	this.description = description;
	this.barcode = barcode;
    }
    
    public Product(String description, EAN13 barcode) {
	this.place = null;
	this.description = description;
	this.barcode = barcode;
    }

    protected Product() {
    }

    public Long getId() {
	return id;
    }

    public void setDescription(String newDescription) {
	this.description = newDescription;
    }

    public Place getPlace() {
	return place;
    }
    
    public String getDescription() {
	return description;
    }

    public boolean equals(Object obj) {
	if (obj == null)
	    return false;
	if (!Product.class.isInstance(obj))
	    return false;
	if (this == obj)
	    return true;
	Product thatProduct = Product.class.cast(obj);
	
	if (thatProduct.id == null || this.id == null)
	    return false;
	
	return Objects.equal(thatProduct.id, this.id);
    }

    public int hashCode() {
	return Objects.hashCode(this.id, this.description, this.barcode);
    }

    public String toString() {
	return Objects.toStringHelper(this) //
		.add("id", id) //
		.add("description", description) //
		.add("barcode", this.barcode)
		.toString();
    }

    public EAN13 getBarcode() {
	return this.barcode;
    }

    public boolean canBeSoldFrom(Place place) {
	return this.place == null || this.place.equals(place);
    }

}