package br.com.precos.domain.place;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import br.com.precos.domain.localization.City;

import com.google.common.base.Objects;

@Entity
public class Place {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private String description;

    @ManyToOne
    @Column(name = "ID_CITY")
    private City city;

    public Place(String description, City city) {
	this.description = description;
	this.city = city;
    }

    protected Place() {
	super();
    }

    public Long getId() {
	return id;
    }

    public void setDescription(String description) {
	this.description = description;
    }

    public String getDescription() {
	return description;
    }

    public City getCity() {
	return city;
    }

    public boolean equals(Object obj) {
	if (obj == null)
	    return false;
	if (!Place.class.isInstance(obj))
	    return false;
	return Objects.equal(Place.class.cast(obj).id, this.id);
    }

    public int hashCode() {
	return Objects.hashCode(this.id, this.description);
    }

    public String toString() {
	return Objects.toStringHelper(this) //
		.add("id", id)
		.add("description", description)
		.toString();
    }
}
