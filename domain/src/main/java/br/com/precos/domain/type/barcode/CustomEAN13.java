package br.com.precos.domain.type.barcode;

import java.math.BigDecimal;
import java.math.BigInteger;

import com.google.common.base.Objects;

import br.com.precos.domain.type.Money;

public class CustomEAN13 extends EAN13 {

    private static final int PRICE_SCALE = 2;
    private static final int PRICE_START_CHAR = 6;

    private String code;
    private Money price;

    CustomEAN13(String code) {
	this.code = code;

	String value = this.code.substring(PRICE_START_CHAR, this.code.length() - 1);
	this.price = new Money(new BigDecimal(new BigInteger(value), PRICE_SCALE));
    }

    @Override
    public String getProductCode() {
	return this.code;
    }

    public String toString() {
	return this.code;
    }

    @Override
    public boolean isSpefic() {
	return true;
    }

    @Override
    public Money getPrice() {
	return this.price;
    }

    @Override
    public boolean equals(Object obj) {
	if (obj == null) return false;
	if (!CustomEAN13.class.isInstance(obj)) return false;
	return obj.toString().equals(this.code);
    }

    @Override
    public int hashCode() {
	return Objects.hashCode(this.code);
    }

}
