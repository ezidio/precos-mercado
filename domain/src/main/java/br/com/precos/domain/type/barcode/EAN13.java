package br.com.precos.domain.type.barcode;

import static com.google.common.base.Preconditions.checkArgument;

import org.apache.commons.validator.routines.CodeValidator;
import org.apache.commons.validator.routines.checkdigit.EAN13CheckDigit;

import br.com.precos.domain.type.Money;

/**
 * Padrão de código de barras
 * 
 */
public abstract class EAN13 {

    private static final char CUSTOM_PRODUCT_CHAR = '2';
    protected static final CodeValidator EAN_VALIDATOR = new CodeValidator((String) null, -1, EAN13CheckDigit.EAN13_CHECK_DIGIT);

    /**
     * Retorna o código do produto
     * 
     * @return
     */
    public abstract String getProductCode();

    /**
     * Retorna o preço do produto
     * 
     * @return
     */
    public abstract Money getPrice();

    /**
     * Diz se o código de barras é padrão ou especifico por estabelecimento.
     * 
     * @return
     */
    public abstract boolean isSpefic();

    /**
     * Cria um novo código de barras conforme o tipo especificado
     * 
     * @param code
     * @return
     */
    public static EAN13 from(String code) {
	checkArgument(code != null, "Código de barras inválido");
	checkArgument(EAN_VALIDATOR.isValid(code), "Código de barras inválido");

	if (code.charAt(0) == CUSTOM_PRODUCT_CHAR) {
	    return new CustomEAN13(code);
	} else {
	    return new DefaultEAN13(code);
	}
    }

}