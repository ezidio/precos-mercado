package br.com.precos.domain.place;

import br.com.precos.infrastructure.repository.IQueryRepository;

public interface IPlaceRepository extends IQueryRepository<Place, Long> {

}
