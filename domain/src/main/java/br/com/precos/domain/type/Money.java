package br.com.precos.domain.type;

import java.math.BigDecimal;

import com.google.common.base.Objects;
import com.google.common.base.Preconditions;

public class Money implements Comparable<Money> {

    public static final int SCALE = 2;
    public static final int ROUND_MODE = BigDecimal.ROUND_HALF_UP;

    public static final Money ZERO = new Money(BigDecimal.ZERO);

    private BigDecimal value;

    public Money(BigDecimal value) {
	Preconditions.checkArgument(value != null, "Valor não pode ser nulo");
	Preconditions.checkArgument(value.compareTo(BigDecimal.ZERO) >= 0, "Valor não pode ser negativo");
	this.value = value.setScale(SCALE, ROUND_MODE);
    }

    @Override
    public int compareTo(Money price) {
	return this.value.compareTo(price.value);
    }

    public BigDecimal toBigDecimal() {
	return this.value;
    }

    public boolean equals(Object obj) {
	if (obj == null)
	    return false;
	if (!Money.class.isInstance(obj))
	    return false;
	return Money.class.cast(obj).value.equals(value);
    }

    public String toString() {
	return this.value.toString();
    }

    public int hashCode() {
	return Objects.hashCode(this.value);
    }

    public Money add(Money other) {
	return new Money(this.value.add(other.value));
    }

    public static Money valueOf(long val) {
	return new Money(BigDecimal.valueOf(val));
    }

    public static Money valueOf(double val) {
	return new Money(BigDecimal.valueOf(val));
    }

    public boolean isZero() {
	return this.equals(Money.ZERO);
    }

    public Money or(Money alternative) {
	return (isZero()) ? alternative : this;
    }

}
