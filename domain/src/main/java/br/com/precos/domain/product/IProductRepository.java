package br.com.precos.domain.product;

import br.com.precos.infrastructure.repository.IQueryRepository;

public interface IProductRepository extends IQueryRepository<Product, Long> {

}
