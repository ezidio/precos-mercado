package br.com.precos.domain.type.barcode;

import br.com.precos.domain.type.Money;

import com.google.common.base.Objects;
import com.google.common.base.Preconditions;

/**
 * Universal Product Code
 * 
 */
public class DefaultEAN13 extends EAN13 {

    private String code;

    DefaultEAN13(String code) {
	Preconditions.checkArgument(EAN_VALIDATOR.isValid(code), "Código de barras inválido");
	this.code = code;
    }

    public String toString() {
	return this.code;
    }

    @Override
    public String getProductCode() {
	return this.code;
    }

    @Override
    public boolean isSpefic() {
	return false;
    }

    @Override
    public Money getPrice() {
	throw new IllegalStateException("Apenas código de barras específicos contêm o preço da mercadoria.");
    }

    @Override
    public boolean equals(Object obj) {
	if (obj == null) return false;
	if (!DefaultEAN13.class.isInstance(obj)) return false;
	return obj.toString().equals(this.code);
    }

    @Override
    public int hashCode() {
	return Objects.hashCode(this.code);
    }
}
