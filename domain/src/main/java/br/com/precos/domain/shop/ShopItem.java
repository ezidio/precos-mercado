package br.com.precos.domain.shop;

import javax.persistence.Column;
import javax.persistence.ManyToOne;

import br.com.precos.domain.product.Product;
import br.com.precos.domain.type.Amount;
import br.com.precos.domain.type.Money;

import com.google.common.base.Objects;
import com.google.common.base.Preconditions;

public class ShopItem {

    @Column(name = "ID_PRODUCT")
    @ManyToOne
    private Product product;
    
    @Column(name = "ID_LIST")
    @ManyToOne
    private ShopList list;

    @Column
    private Money price;

    @Column
    private Amount amount;

    protected ShopItem() {
	// Construtor vazio para o ORM
    }
    
    public ShopItem(Product product, Money price, Amount amount) {
	Preconditions.checkArgument(product != null, "Produto deve ser especificado.");
	Preconditions.checkArgument(price != null, "O preço deve ser especificado.");
	Preconditions.checkArgument(amount != null, "A quantidade deve ser especificada.");
	this.product = product;
	this.price = price;
	this.amount = amount;
    }


    public static ShopItem createWith(Product product) {
	return new ShopItem(product, Money.ZERO, Amount.ONE);
    }

    public Product getProduct() {
	return product;
    }

    public Money getPrice() {
	return price;
    }

    public Amount getAmount() {
	return amount;
    }

    public Money getTotal() {
	return new Money(this.amount.toBigDecimal().multiply(this.price.toBigDecimal()));
    }

    public boolean equals(Object obj) {
	if (obj == null)
	    return false;
	if (!ShopItem.class.isInstance(obj))
	    return false;

	ShopItem that = ShopItem.class.cast(obj);
	return Objects.equal(that.hashCode(), this.hashCode());
    }

    public int hashCode() {
	return Objects.hashCode(this.product.getId(), this.price, this.amount);
    }

    public String toString() {
	return Objects.toStringHelper(this).add("product", product).add("price", price).add("amount", amount).toString();
    }

    public ShopItem setPrice(Money newPrice) {
	Preconditions.checkArgument(newPrice != null, "Preço não pode ser nulo");
	return new ShopItem(product, newPrice, amount);
    }

    public ShopItem setAmount(Amount newAmount) {
	Preconditions.checkArgument(newAmount != null, "Quantidade não pode ser nula");
	Preconditions.checkArgument(!newAmount.isZero(), "Quantidade não pode ser zero");
	return new ShopItem(product, price, newAmount);
    }

    public ShopItem setTotal(Money newTotal) {
	Preconditions.checkArgument(newTotal != null, "Preço não pode ser nulo");

	if (price.equals(Money.ZERO)) {
	    Money newPrice = new Money(newTotal.toBigDecimal().divide(amount.toBigDecimal(), Money.ROUND_MODE));
	    return new ShopItem(product, newPrice, amount);
	} else {
	    Amount newAmount = new Amount(newTotal.toBigDecimal().divide(price.toBigDecimal(), Amount.SCALE, Amount.ROUND_MODE));
	    return new ShopItem(product, price, newAmount);
	}
    }

    public ShopItem updateValues(Money newPrice, Amount newAmount) {
	return new ShopItem(product, newPrice, newAmount);
    }

    public boolean isValid() {
	return !this.price.isZero() && !this.amount.isZero();
    }


}
