package br.com.precos.domain.shop;

public enum ShopListStatus {

    DRAFT, CONCLUDED, CANCELLED;

}
