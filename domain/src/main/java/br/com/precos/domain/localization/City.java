package br.com.precos.domain.localization;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.google.common.base.Objects;

@Entity
public class City {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private String name;

    @Column(name = "ID_STATE")
    @ManyToOne
    private State state;

    protected City() {
	// Necessário para o Hibernate
    }

    public City(String name, State state) {
	this.name = name;
	this.state = state;
    }

    public Long getId() {
	return id;
    }

    public String getName() {
	return name;
    }

    public State getState() {
	return state;
    }

    public boolean equals(Object obj) {
	if (obj == null)
	    return false;
	if (!City.class.isInstance(obj))
	    return false;
	return Objects.equal(City.class.cast(obj).id, this.id);
    }

    public int hashCode() {
	return Objects.hashCode(this.id, this.name, this.state);
    }

    public String toString() {
	return Objects.toStringHelper(this) //
		.add("id", id).add("name", name).add("state", state).toString();
    }

}
