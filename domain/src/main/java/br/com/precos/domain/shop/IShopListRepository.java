package br.com.precos.domain.shop;

import br.com.precos.infrastructure.repository.IQueryRepository;

public interface IShopListRepository extends IQueryRepository<ShopList, Long> {

    void save(ShopList list);

}
