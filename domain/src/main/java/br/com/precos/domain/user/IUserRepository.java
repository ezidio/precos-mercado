package br.com.precos.domain.user;

import br.com.precos.infrastructure.repository.IQueryRepository;


public interface IUserRepository extends IQueryRepository<User, Long> {
    
    
}
