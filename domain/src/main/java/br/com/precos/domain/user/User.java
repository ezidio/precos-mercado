package br.com.precos.domain.user;

import javax.persistence.Column;
import javax.persistence.Id;

import br.com.precos.domain.type.Email;

import com.google.common.base.Objects;
import com.google.common.base.Preconditions;

public class User {
    
    @Id
    private Long id;
    
    @Column
    private String name;
    
    @Column
    private Email email;

    protected User() {
	super();
    }

    private User(Builder builder) {
	this.name = builder.name;
	this.email = builder.email;
    }

    public static Builder builder() {
	return new Builder();
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public Long getId() {
	return id;
    }

    public Email getEmail() {
	return email;
    }

    public boolean equals(Object obj) {
	if (obj == null)
	    return false;
	if (!User.class.isInstance(obj))
	    return false;
	return Objects.equal(User.class.cast(obj).id, this.id);
    }

    public int hashCode() {
	return Objects.hashCode(this.id, this.name, this.email);
    }

    public String toString() {
	return Objects.toStringHelper(this) //
		.add("id", id) //
		.add("name", name)//
		.add("email", email)//
		.toString();
    }

    /**
     * User Builder
     */
    public static final class Builder {

	private String name;
	private Email email;

	public static Builder create() {
	    return new Builder();
	}

	public Builder name(String name) {
	    this.name = name;
	    return this;
	}

	public Builder email(Email email) {
	    this.email = email;
	    return this;
	}

	public User build() {
	    Preconditions.checkArgument(this.name != null);
	    Preconditions.checkArgument(this.email != null);

	    return new User(this);
	}

    }
}
