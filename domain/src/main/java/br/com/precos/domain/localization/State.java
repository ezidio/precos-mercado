package br.com.precos.domain.localization;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.google.common.base.Objects;

@Entity
public class State {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private String name;

    @Column
    private String abbreviation;

    protected State() {
    }

    public State(String abbreviation, String name) {
	this.abbreviation = abbreviation;
	this.name = name;
    }

    public Long getId() {
	return id;
    }

    public String getName() {
	return name;
    }

    public String getAbbreviation() {
	return abbreviation;
    }

    public boolean equals(Object obj) {
	if (obj == null)
	    return false;
	if (!State.class.isInstance(obj))
	    return false;
	return Objects.equal(State.class.cast(obj).id, this.id);
    }

    public int hashCode() {
	return Objects.hashCode(this.id, this.name, this.abbreviation);
    }

    public String toString() {
	return Objects.toStringHelper(this) //
		.add("id", id).add("name", name).add("abbreviation", abbreviation).toString();
    }

}
