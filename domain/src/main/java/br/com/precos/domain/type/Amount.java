package br.com.precos.domain.type;

import java.math.BigDecimal;

import com.google.common.base.Objects;
import com.google.common.base.Preconditions;

public class Amount implements Comparable<Amount> {

    public static final int SCALE = 3;
    public static final int ROUND_MODE = BigDecimal.ROUND_HALF_UP;

    public static final Amount ZERO = new Amount(BigDecimal.ZERO);

    public static final Amount ONE = new Amount(BigDecimal.ONE);

    private BigDecimal value;

    public Amount(BigDecimal value) {
	Preconditions.checkArgument(value != null, "Valor não pode ser nulo");
	Preconditions.checkArgument(value.compareTo(BigDecimal.ZERO) >= 0, "Valor não pode ser negativo");
	this.value = value.setScale(SCALE);
    }

    @Override
    public int compareTo(Amount amount) {
	return this.value.compareTo(amount.value);
    }

    public BigDecimal toBigDecimal() {
	return this.value;
    }

    public boolean equals(Object obj) {
	if (obj == null)
	    return false;
	if (!Amount.class.isInstance(obj))
	    return false;
	return Amount.class.cast(obj).value.equals(value);
    }

    public int hashCode() {
	return Objects.hashCode(this.value);
    }

    public String toString() {
	return this.value.toString();
    }

    public static Amount valueOf(int value) {
	return new Amount(BigDecimal.valueOf(value));
    }

    public static Amount valueOf(double value) {
	return new Amount(BigDecimal.valueOf(value));
    }

    public boolean isZero() {
	return this.equals(Amount.ZERO);
    }

    public Amount or(Amount alternative) {
	return (isZero()) ? alternative : this;
    }

}
