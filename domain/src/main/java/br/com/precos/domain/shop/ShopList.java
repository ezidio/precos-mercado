package br.com.precos.domain.shop;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.joda.time.DateTime;

import br.com.precos.domain.place.Place;
import br.com.precos.domain.product.Product;
import br.com.precos.domain.type.Money;
import br.com.precos.domain.user.User;

import com.google.common.base.Objects;
import com.google.common.base.Optional;
import com.j256.ormlite.field.ForeignCollectionField;

/**
 * Lista de compras
 * 
 * @author Everton Tavares
 * 
 */
public class ShopList {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @Column(name = "ID_PLACE")
    private Place place;

    @Column
    private DateTime date;

    @ManyToOne
    @Column(name = "ID_USER")
    private User user;

    @Column
    @Enumerated(EnumType.STRING)
    private ShopListStatus status;

    @ForeignCollectionField
    private Collection<ShopItem> items;

    protected ShopList() {
	// Construtor vazio para o ORM
    }

    public ShopList(User user, Place place) {
	this.user = user;
	this.place = place;
	this.date = DateTime.now();
	this.items = new ArrayList<>();
	this.status = ShopListStatus.DRAFT;
    }

    public void addItem(ShopItem item) {
	checkArgument(item.getProduct().canBeSoldFrom(this.place));
	checkState(isDraft(), "Esta lista não pode ser alterada.");
	
	removeItemIfExists(item);
	
	this.items.add(item);
    }

    public void removeItemIfExists(ShopItem item) {
	Optional<ShopItem> existingItem = this.getItemBy(item.getProduct());
	if (existingItem.isPresent()) {
	    this.items.remove(existingItem.get());
	}
    }

    public void conclude() {
	checkState(isDraft(), "Esta lista não pode ser finalizada.");
	checkState(hasItems(), "A lista deve conter items para ser finalizada.");
	checkState(allItemsIsValid(), "A lista contem itens inválidos.");
	this.status = ShopListStatus.CONCLUDED;
    }

    public Money getTotalPrice() {
	Money total = Money.ZERO;
	for (ShopItem item : this.items) {
	    total = item.getTotal().add(total);
	}
	return total;
    }

    public Long getId() {
	return id;
    }

    public Place getPlace() {
	return place;
    }

    public DateTime getDate() {
	return date;
    }

    public User getUser() {
	return user;
    }

    public ShopListStatus getStatus() {
	return this.status;
    }

    public boolean contains(Product product) {
	return this.getItemBy(product).isPresent();
    }

    public Optional<ShopItem> getItemBy(Product product) {
	for (ShopItem item : this.items) {
	    if (item.getProduct().equals(product)) {
		return Optional.of(item);
	    }
	}
	return Optional.absent();
    }

    public Collection<ShopItem> getItems() {
	return Collections.unmodifiableCollection(this.items);
    }

    public boolean isDraft() {
	return ShopListStatus.DRAFT.equals(this.status);
    }

    public boolean isCancelled() {
	return ShopListStatus.CANCELLED.equals(this.status);
    }

    public boolean hasItems() {
	return items.size() > 0;
    }

    public boolean allItemsIsValid() {
	for (ShopItem item : this.items) {
	    if (!item.isValid())
		return false;
	}
	return true;
    }

    public void cancel() {
	this.status = ShopListStatus.CANCELLED;
    }

    public boolean equals(Object obj) {
	if (obj == null)
	    return false;
	if (!ShopList.class.isInstance(obj))
	    return false;
	return ShopList.class.cast(obj).id.equals(this.id);
    }

    @Override
    public int hashCode() {
	return Objects.hashCode(this.id);
    }

    @Override
    public String toString() {
	return Objects.toStringHelper(getClass()).add("id", id).add("user", user).add("place", place).add("date", date).add("status", status).toString();
    }

}
