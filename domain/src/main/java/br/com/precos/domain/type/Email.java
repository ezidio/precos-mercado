package br.com.precos.domain.type;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.regex.Pattern;

import com.google.common.base.Objects;

public class Email {

    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    private String value;

    public Email(String value) {
	checkArgument(value != null, "Email can not have null value");
	checkArgument(Pattern.matches(EMAIL_PATTERN, value), "Email pattern is invalid");
	this.value = value;
    }

    public String toString() {
	return this.value;
    }

    public int hashCode() {
	return Objects.hashCode(this.value);
    }

    public boolean equals(Object obj) {
	if (obj == null)
	    return false;
	if (!Email.class.isInstance(obj))
	    return false;
	return Email.class.cast(obj).value.equals(value);
    }

}
