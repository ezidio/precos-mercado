package br.com.precos.infrastructure.repository.exception;

public class RepositoryException extends RuntimeException {

    private static final long serialVersionUID = 7059628454513920999L;

    public RepositoryException() {
	super();
    }

    public RepositoryException(String arg0, Throwable arg1) {
	super(arg0, arg1);
    }
    
    public RepositoryException(Throwable arg1) {
	super(arg1);
    }

}
