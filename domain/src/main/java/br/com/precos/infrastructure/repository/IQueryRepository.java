package br.com.precos.infrastructure.repository;

import java.util.List;

import com.google.common.base.Optional;

public interface IQueryRepository<T, ID> {

    Optional<T> getById(ID id);

    List<T> getAll();

}