package br.com.precos.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import br.com.precos.android.R;

import com.actionbarsherlock.app.SherlockActivity;

public class LoginActivity extends SherlockActivity {

	private Button buttonLogin;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

		bindComponents();
	}

	private void bindComponents() {
		this.buttonLogin = (Button) findViewById(R.id.activity_login_buttonLogin);
		this.buttonLogin.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent(LoginActivity.this,
						ShopListActivity.class);
				startActivity(intent);
			}
		});
	}

}
