package br.com.precos.android.activity;

import android.os.Bundle;
import br.com.precos.android.R;

import com.actionbarsherlock.app.SherlockActivity;

public class ShopListActivity extends SherlockActivity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_shop_list);
	}

}
