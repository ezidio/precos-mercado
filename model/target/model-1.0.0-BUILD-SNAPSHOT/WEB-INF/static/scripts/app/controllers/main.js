define(['angular', 'angular-route','gumga/components', 'gumga/components/table', 'gumga/components/pagination' ], function(angular) {
	'use strict';
	
	return angular
	  .module('webComponentsApp', ['gumga.components', 'gumga.components.grid', 'gumga.components.pagination', 'ngRoute'])


	  .controller('MainCtrl', ['$scope', '$location', function ($scope, $location) {

		$scope.awesomeThings = [
		  'HTML5 Boilerplate',
		  'AngularJS',
		  'Karma'
		];

		$scope.moneyValue = 100.23;
		$scope.maskValue = "aa";

		$scope.selection = [];

		$scope.gridColumns = [
			{"label" : "Coluna 1", "field" : "col1"},
			{"label" : "Coluna 2", "field" : "col2"}
		];

		$scope.$watch(function () { return $location.url(); }, function (url) {
		    if (url) {
		        var search = $location.search();
		        var pageSize = search.pageSize || 10;
		        var start = search.start || 0;

		        $scope.gridValues.start = start;
		        $scope.gridValues.pageSize = pageSize;

		        var lista = [];

		        for (var i = 0; i < pageSize; i++) {
		        	var id = i + parseInt(start);
		        	lista.push({'id' : id, 'col1' : '1 - '+id, 'col2' : '2 - '+id});
		        };

		        $scope.gridValues.values = lista;
		    }
		});

		$scope.gridValues = {
			'start' : 0,
			'pageSize' : 10,
			'count' : 500,
			'values' : [
				{'id' : 1, 'col1' : 'A', 'col2' : 'B'},
				{'id' : 2, 'col1' : 'C', 'col2' : 'D'},
				{'id' : 3, 'col1' : 'E', 'col2' : 'F'},
				{'id' : 4, 'col1' : 'G', 'col2' : 'H'},
				{'id' : 5, 'col1' : 'I', 'col2' : 'J'}
			]
		};
		
	  }]);


})