package br.com.precos.domain.shop;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.List;

import org.joda.time.LocalDateTime;
import org.junit.Test;
import org.mockito.Mockito;

import br.com.precos.domain.product.Product;
import br.com.precos.domain.type.Amount;
import br.com.precos.domain.type.Money;
import br.com.precos.domain.type.barcode.EAN13;

import com.google.common.base.Optional;
import com.google.common.collect.Lists;

public class ShopListServiceTest {

    private static final EAN13 VALID_BARCODE = EAN13.from("7896007941636");
    
    @Test
    public void should_retrieve_the_last_shopitem() {
	ShopListService service = new ShopListService();
	Product product = new Product("Maizena", VALID_BARCODE);
	Optional<ShopItem> shopItemFound = Optional.of(new ShopItem(product, Money.valueOf(20), Amount.ONE));

	ShopList list1 = Mockito.mock(ShopList.class);
	when(list1.getDate()).thenReturn(LocalDateTime.parse("2014-01-01"));
	when(list1.contains(product)).thenReturn(Boolean.TRUE);
	ShopList list2 = Mockito.mock(ShopList.class);
	when(list2.getDate()).thenReturn(LocalDateTime.parse("2014-01-02"));
	when(list2.contains(product)).thenReturn(Boolean.FALSE);
	ShopList list3 = Mockito.mock(ShopList.class);
	when(list3.getDate()).thenReturn(LocalDateTime.parse("2014-01-03"));
	when(list3.contains(product)).thenReturn(Boolean.FALSE);
	ShopList list4 = Mockito.mock(ShopList.class);
	when(list4.getDate()).thenReturn(LocalDateTime.parse("2014-01-04"));
	when(list4.contains(product)).thenReturn(Boolean.TRUE);
	when(list4.getItemBy(product)).thenReturn(shopItemFound);

	List<ShopList> lists = Lists.newArrayList(list1, list2, list3, list4);

	Optional<ShopItem> item = service.getLastPrice(product, lists);

	assertNotNull(item);
	assertTrue(item.isPresent());
	assertEquals(shopItemFound.get(), item.get());

    }

    @Test
    public void should_retrieve_absent_if_is_not_present() {
	ShopListService service = new ShopListService();
	Product product = new Product("Maizena", VALID_BARCODE);
	Optional<ShopItem> shopItemFound = Optional.of(new ShopItem(product, Money.valueOf(20), Amount.ONE));

	ShopList list1 = Mockito.mock(ShopList.class);
	when(list1.getDate()).thenReturn(LocalDateTime.parse("2014-01-01"));
	when(list1.contains(product)).thenReturn(Boolean.FALSE);
	ShopList list2 = Mockito.mock(ShopList.class);
	when(list2.getDate()).thenReturn(LocalDateTime.parse("2014-01-02"));
	when(list2.contains(product)).thenReturn(Boolean.FALSE);
	ShopList list3 = Mockito.mock(ShopList.class);
	when(list3.getDate()).thenReturn(LocalDateTime.parse("2014-01-03"));
	when(list3.contains(product)).thenReturn(Boolean.FALSE);
	ShopList list4 = Mockito.mock(ShopList.class);
	when(list4.getDate()).thenReturn(LocalDateTime.parse("2014-01-04"));
	when(list4.contains(product)).thenReturn(Boolean.FALSE);
	when(list4.getItemBy(product)).thenReturn(shopItemFound);

	List<ShopList> lists = Lists.newArrayList(list1, list2, list3, list4);

	Optional<ShopItem> item = service.getLastPrice(product, lists);

	assertNotNull(item);
	assertFalse(item.isPresent());

    }
    
    @Test
    public void should_not_get_item_from_cancelled_list() {
	ShopListService service = new ShopListService();
	Product product = new Product("Maizena", VALID_BARCODE);
	Optional<ShopItem> shopItemFound = Optional.of(new ShopItem(product, Money.valueOf(20), Amount.ONE));

	ShopList list1 = Mockito.mock(ShopList.class);
	when(list1.getDate()).thenReturn(LocalDateTime.parse("2014-01-01"));
	when(list1.contains(product)).thenReturn(Boolean.TRUE);
	when(list1.getItemBy(product)).thenReturn(shopItemFound);
	when(list1.isCancelled()).thenReturn(false);
	
	ShopList list2 = Mockito.mock(ShopList.class);
	when(list2.getDate()).thenReturn(LocalDateTime.parse("2014-01-02"));
	when(list2.contains(product)).thenReturn(Boolean.FALSE);
	when(list2.isCancelled()).thenReturn(false);
	
	ShopList list3 = Mockito.mock(ShopList.class);
	when(list3.getDate()).thenReturn(LocalDateTime.parse("2014-01-03"));
	when(list3.contains(product)).thenReturn(Boolean.FALSE);
	when(list3.isCancelled()).thenReturn(false);
	
	ShopList list4 = Mockito.mock(ShopList.class);
	when(list4.getDate()).thenReturn(LocalDateTime.parse("2014-01-04"));
	when(list4.contains(product)).thenReturn(Boolean.TRUE);
	when(list4.isCancelled()).thenReturn(true);

	List<ShopList> lists = Lists.newArrayList(list1, list2, list3, list4);

	Optional<ShopItem> item = service.getLastPrice(product, lists);

	assertNotNull(item);
	assertTrue(item.isPresent());
	assertEquals(shopItemFound.get(), item.get());
	
    }

}
