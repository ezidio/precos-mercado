package br.com.precos.domain.product;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import org.junit.Assert;
import org.junit.Test;

import br.com.precos.domain.type.barcode.EAN13;

public class ProductTest {

    private static final String VALID_BARCODE = "7896007941636";

    @Test
    public void should_be_created_without_id() throws NoSuchMethodException, SecurityException {
	Product product = new Product();
	Modifier.isProtected(Product.class.getDeclaredConstructor().getModifiers());
	assertNull(product.getId());
    }

    @Test
    public void should_retrieve_the_id() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
	Product product = new Product();

	Field field = Product.class.getDeclaredField("id");
	field.setAccessible(true);
	field.set(product, Long.valueOf(20L));

	assertEquals(Long.valueOf(20), product.getId());
    }

    @Test
    public void should_be_created_with_the_description() {

	Product product = new Product("Maizena", EAN13.from(VALID_BARCODE));
	assertEquals("Maizena", product.getDescription());
	assertEquals(EAN13.from(VALID_BARCODE), product.getBarcode());
	
    }

    @Test
    public void should_update_the_description() {
	Product product = new Product("Maizena", EAN13.from(VALID_BARCODE));
	assertEquals("Maizena", product.getDescription());

	product.setDescription("Maizena Duryea");
	assertEquals("Maizena Duryea", product.getDescription());
    }

    @Test
    public void should_show_the_toString() {
	assertEquals("Product{id=null, description=Maizena, barcode=7896007941636}", new Product("Maizena", EAN13.from(VALID_BARCODE)).toString());
    }

    @Test
    public void should_has_equals_hash_with_equals_values() {
	Product product = new Product("Maizena", EAN13.from(VALID_BARCODE));
	Product product1 = new Product("Maizena", EAN13.from(VALID_BARCODE));
	assertEquals(product.hashCode(), product1.hashCode());
    }
    
    @Test
    public void should_has_different_hash_with_different_descriptions() {
	Product product = new Product("Maizena", EAN13.from(VALID_BARCODE));
	Product product1 = new Product("Sabão", EAN13.from(VALID_BARCODE));
	Assert.assertNotEquals(product.hashCode(), product1.hashCode());
    }
    
    @Test
    public void should_has_different_hash_with_different_barcodes() {
	Product product = new Product("Maizena", EAN13.from("2199100003115"));
	Product product1 = new Product("Maizena", EAN13.from(VALID_BARCODE));
	Assert.assertNotEquals(product.hashCode(), product1.hashCode());
    }

}
