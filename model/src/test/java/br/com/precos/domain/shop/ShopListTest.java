package br.com.precos.domain.shop;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.joda.time.LocalDate;
import org.junit.Test;
import org.mockito.Mockito;

import br.com.precos.domain.place.Place;
import br.com.precos.domain.product.Product;
import br.com.precos.domain.type.Amount;
import br.com.precos.domain.type.Money;
import br.com.precos.domain.type.barcode.EAN13;
import br.com.precos.domain.user.User;

public class ShopListTest {
    private static final EAN13 VALID_BARCODE = EAN13.from("7896007941636");
    @Test
    public void should_be_created_with_empty_item_list() {
	User user = Mockito.mock(User.class);
	Place place = Mockito.mock(Place.class);
	ShopList list = new ShopList(user, place);

	assertEquals(user, list.getUser());
	assertEquals(place, list.getPlace());
	assertEquals(LocalDate.now(), list.getDate().toLocalDate());
	assertNotNull(list.getItems());

	assertEquals(0, list.getItems().size());
	assertEquals(ShopListStatus.DRAFT, list.getStatus());

	assertEquals(Money.ZERO, list.getTotalPrice());
    }

    @Test
    public void should_receive_items_and_update_the_total() {
	User user = Mockito.mock(User.class);
	Place place = Mockito.mock(Place.class);
	ShopList list = new ShopList(user, place);

	Product product = Mockito.mock(Product.class);
	list.addItem(new ShopItem(product, Money.valueOf(2), Amount.valueOf(2)));

	assertEquals(1, list.getItems().size());
	assertEquals(Money.valueOf(4), list.getTotalPrice());

	Product anotherProduct = Mockito.mock(Product.class);
	list.addItem(new ShopItem(anotherProduct, Money.valueOf(1.4), Amount.valueOf(2)));

	assertEquals(2, list.getItems().size());
	assertEquals(Money.valueOf(6.8), list.getTotalPrice());
    }
    
    @Test(expected = IllegalStateException.class)
    public void should_not_receive_items_if_cancelled() {
	User user = Mockito.mock(User.class);
	Place place = Mockito.mock(Place.class);
	ShopList list = new ShopList(user, place);
	
	Product product = Mockito.mock(Product.class);
	list.addItem(new ShopItem(product, Money.valueOf(2), Amount.valueOf(2)));
	
	list.cancel();
	
	Product anotherProduct = Mockito.mock(Product.class);
	list.addItem(new ShopItem(anotherProduct, Money.valueOf(2), Amount.valueOf(2)));
	
    }
    
    @Test(expected = IllegalStateException.class)
    public void should_not_receive_items_if_conclued() {
	User user = Mockito.mock(User.class);
	Place place = Mockito.mock(Place.class);
	ShopList list = new ShopList(user, place);
	
	Product product = Mockito.mock(Product.class);
	list.addItem(new ShopItem(product, Money.valueOf(2), Amount.valueOf(2)));
	
	list.conclude();
	
	Product anotherProduct = Mockito.mock(Product.class);
	list.addItem(new ShopItem(anotherProduct, Money.valueOf(2), Amount.valueOf(2)));
	
    }

    @Test
    public void should_be_finalized_if_have_only_valid_items() {
	User user = Mockito.mock(User.class);
	Place place = Mockito.mock(Place.class);
	ShopList list = new ShopList(user, place);

	Product product = Mockito.mock(Product.class);
	list.addItem(new ShopItem(product, Money.valueOf(2), Amount.valueOf(2)));

	list.conclude();

	assertEquals(ShopListStatus.CONCLUDED, list.getStatus());
    }

    @Test(expected = IllegalStateException.class)
    public void should_not_be_finalized_if_no_have_items() {
	User user = Mockito.mock(User.class);
	Place place = Mockito.mock(Place.class);
	ShopList list = new ShopList(user, place);

	list.conclude();
    }

    @Test(expected = IllegalStateException.class)
    public void should_not_be_finalized_if_have_invalid_items() {
	User user = Mockito.mock(User.class);
	Place place = Mockito.mock(Place.class);
	ShopList list = new ShopList(user, place);

	Product product = Mockito.mock(Product.class);
	list.addItem(ShopItem.createWith(product));

	Product anotherProduct = Mockito.mock(Product.class);
	list.addItem(ShopItem.createWith(anotherProduct).setPrice(Money.valueOf(10)));

	list.conclude();
    }

    @Test(expected = IllegalStateException.class)
    public void should_not_be_finalized_if_list_is_cancelled() {
	User user = Mockito.mock(User.class);
	Place place = Mockito.mock(Place.class);
	ShopList list = new ShopList(user, place);

	Product anotherProduct = Mockito.mock(Product.class);
	list.addItem(ShopItem.createWith(anotherProduct).setPrice(Money.valueOf(10)));

	list.cancel();
	list.conclude();
    }

    @Test
    public void should_be_cancelled() {
	User user = Mockito.mock(User.class);
	Place place = Mockito.mock(Place.class);
	ShopList list = new ShopList(user, place);

	list.cancel();

	assertEquals(ShopListStatus.CANCELLED, list.getStatus());
    }

    @Test
    public void should_replace_values_for_the_same_product() {
	User user = Mockito.mock(User.class);
	Place place = Mockito.mock(Place.class);
	ShopList list = new ShopList(user, place);

	Product product = Mockito.mock(Product.class);
	list.addItem(new ShopItem(product, Money.valueOf(2), Amount.valueOf(2)));

	assertNotNull(list.getItems());
	assertEquals(1, list.getItems().size());
	assertEquals(Money.valueOf(4), list.getTotalPrice());

	list.addItem(new ShopItem(product, Money.valueOf(2), Amount.valueOf(4)));

	assertNotNull(list.getItems());
	assertEquals(1, list.getItems().size());
	assertEquals(Money.valueOf(8), list.getTotalPrice());
    }

    @Test
    public void should_retrieve_if_the_list_contains_a_product() {
	User user = Mockito.mock(User.class);
	Place place = Mockito.mock(Place.class);
	ShopList list = new ShopList(user, place);

	Product product = new Product("Maizena", VALID_BARCODE);
	list.addItem(new ShopItem(product, Money.valueOf(2), Amount.valueOf(2)));

	assertTrue(list.contains(product));
	assertFalse(list.contains(new Product("Leite Desnatado", VALID_BARCODE)));
    }

    @Test
    public void should_retrieve_the_item_associated_with_the_product() {
	User user = Mockito.mock(User.class);
	Place place = Mockito.mock(Place.class);
	ShopList list = new ShopList(user, place);

	Product product = new Product("Maizena", VALID_BARCODE);
	ShopItem item = new ShopItem(product, Money.valueOf(2), Amount.valueOf(2));
	list.addItem(item);

	assertTrue(list.getItemBy(product).isPresent());
	assertEquals(item, list.getItemBy(product).get());

	assertFalse(list.getItemBy(new Product("Sabão", VALID_BARCODE)).isPresent());
    }

}
