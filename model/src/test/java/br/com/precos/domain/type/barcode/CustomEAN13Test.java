package br.com.precos.domain.type.barcode;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import br.com.precos.domain.type.Money;

public class CustomEAN13Test {

    CustomEAN13 boloSimples = new CustomEAN13("2199100003115");

    @Test(expected = IllegalArgumentException.class)
    public void should_throw_exception_if_create_with_invalid_code() {
	new DefaultEAN13("2199100003111");
    }

    @Test
    public void should_toString_retrieve_the_code() {
	assertEquals("2199100003115", this.boloSimples.toString());
    }

    @Test
    public void should_retrieve_the_product_code() {
	assertEquals("2199100003115", this.boloSimples.getProductCode());
    }
    
    @Test
    public void should_throw_exception_if_get_price() {
	assertEquals(Money.valueOf(3.11), this.boloSimples.getPrice());
    }
    
}
