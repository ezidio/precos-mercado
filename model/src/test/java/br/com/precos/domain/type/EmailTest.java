package br.com.precos.domain.type;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class EmailTest {

	@Test(expected = IllegalArgumentException.class)
	public void should_throw_a_exception_when_created_with_a_null_email() {
		new Email(null);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void should_throw_a_exception_when_created_with_a_invalid_email() {
		new Email("email_invalido");
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void should_throw_a_exception_when_created_with_a_incomplete_email() {
		new Email("email@servidor");
	}
	
	@Test
	public void should_be_created_with_valid_email() {
		Email email = new Email("email@servidor.com");
		assertEquals("email@servidor.com", email.toString());
	}
	
	@Test
	public void should_equals_value_be_equals() {
				
		assertTrue(new Email("email@servidor.com").equals(new Email("email@servidor.com")));
		assertFalse(new Email("email@servidor.com").equals(null));
		assertFalse(new Email("email@servidor.com").equals("outro objeto qualquer"));
		assertFalse(new Email("email@servidor.com").equals(new Email("email@servidor.com.br")));
		
	}
	
	@Test
	public void should_have_equals_hash_to_equals_values() {
		assertEquals(new Email("email@servidor.com").hashCode(), new Email("email@servidor.com").hashCode());
		assertEquals(new Email("jose@email.com").hashCode(), new Email("jose@email.com").hashCode());
		assertEquals(new Email("carlos@email.com.br").hashCode(), new Email("carlos@email.com.br").hashCode());
	}

}
