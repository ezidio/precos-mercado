package br.com.precos.domain.type;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

import org.junit.Test;

public class MoneyTest {

    @Test
    public void testCreate() {
	Money price = new Money(BigDecimal.ONE);
	assertEquals(BigDecimal.ONE.setScale(Money.SCALE), price.toBigDecimal());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateWithNull() {
	new Money(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateWithNegative() {
	new Money(BigDecimal.valueOf(-1));
    }

    @Test
    public void should_create_with_zero() {
	Money price = new Money(BigDecimal.ZERO);
	assertEquals(BigDecimal.ZERO.setScale(Money.SCALE), price.toBigDecimal());
    }
    
    @Test
    public void should_round_when_overflow_scale() {
	assertEquals(Money.valueOf(2.35), Money.valueOf(2.345));
	assertEquals(Money.valueOf(2.34), Money.valueOf(2.344));
	assertEquals(Money.valueOf(2.35), Money.valueOf(2.349));
    }

    @Test
    public void testZero() {
	assertEquals(BigDecimal.ZERO.setScale(Money.SCALE), Money.ZERO.toBigDecimal());
    }

    @Test
    public void testEquals() {
	Money price = new Money(BigDecimal.TEN);

	assertTrue(price.equals(new Money(BigDecimal.TEN)));
	assertFalse(price.equals(null));
	assertFalse(price.equals("string nada a ver"));

	assertTrue(new Money(BigDecimal.valueOf(2.2).setScale(20)).equals(new Money(BigDecimal.valueOf(2.20).setScale(2))));
    }

    @Test
    public void testAdd() {
	assertEquals(Money.valueOf(2), Money.valueOf(1).add(Money.valueOf(1)));
	assertEquals(Money.valueOf(3), Money.valueOf(1.5).add(Money.valueOf(1.5)));
	assertEquals(Money.valueOf(134.86), Money.valueOf(100.43).add(Money.valueOf(34.43)));
    }

    @Test
    public void testValueOfLong() {
	Money price = Money.valueOf(2);
	assertEquals(new Money(BigDecimal.valueOf(2)), price);
    }

    @Test
    public void testValueOfDouble() {
	Money price = Money.valueOf(2.2);
	assertEquals(new Money(BigDecimal.valueOf(2.2)), price);
    }

    @Test
    public void should_retrieve_the_string_value() {
	assertEquals("2.20", Money.valueOf(2.2).toString());
	assertEquals("234.43", Money.valueOf(234.43).toString());
    }

    @Test
    public void should_equals_value_be_equals() {

	assertTrue(Money.valueOf(2).equals(Money.valueOf(2.0)));
	assertFalse(Money.valueOf(2).equals(null));
	assertFalse(Money.valueOf(2).equals("outro objeto qualquer"));
	assertFalse(Money.valueOf(2).equals(Money.valueOf(3)));

    }

    @Test
    public void should_have_equals_hash_to_equals_values() {
	assertEquals(Money.valueOf(2).hashCode(), Money.valueOf(2.0).hashCode());
	assertEquals(Money.valueOf(1.3).hashCode(), Money.valueOf(1.3).hashCode());
	assertEquals(Money.valueOf(2.34).hashCode(), Money.valueOf(2.34).hashCode());
	assertEquals(Money.valueOf(2843984).hashCode(), Money.valueOf(2843984).hashCode());
    }
    
}
