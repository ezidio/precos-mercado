package br.com.precos.infra.dao;

import java.util.List;

import br.com.precos.service.ListResult;

import com.google.common.base.Optional;
import com.mysema.query.types.Expression;
import com.mysema.query.types.expr.BooleanExpression;

public interface IDAO<E, I> {

	public abstract <T> T findById(Expression<T> projection, I id);

	public abstract E findById(I id);

	public abstract <T> ListResult<T> search(Expression<T> projection,
			Optional<BooleanExpression> expr, long start, long pageSize);

	public abstract ListResult<E> search(Optional<BooleanExpression> expr,
			long start, long pageSize);

	public abstract <T> List<T> list(Expression<T> projection,
			Optional<BooleanExpression> expr);

	public abstract List<E> list(Optional<BooleanExpression> expr);

	public abstract <T> List<T> list(Expression<T> projection);

	public abstract List<E> list();

}