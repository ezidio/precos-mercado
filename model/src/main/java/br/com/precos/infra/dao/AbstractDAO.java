package br.com.precos.infra.dao;

import java.util.List;

import br.com.precos.service.ListResult;

import com.google.common.base.Optional;
import com.mysema.query.types.EntityPath;
import com.mysema.query.types.Expression;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.expr.SimpleExpression;

public abstract class AbstractDAO<E, I> implements IDAO<E, I> {

    protected EntityPath<E> entity;
    protected SimpleExpression<I> idProperty;

    @Override
    public E findById(I id) {
	return findById(entity, id);
    }

    @Override
    public ListResult<E> search(Optional<BooleanExpression> expr, long start, long pageSize) {
	return this.search(entity, expr, start, pageSize);
    }

    @Override
    public List<E> list(Optional<BooleanExpression> expr) {
	return list(entity, expr);
    }

    @Override
    public <T> List<T> list(Expression<T> projection) {
	return list(projection, Optional.<BooleanExpression> absent());
    }

    @Override
    public List<E> list() {
	return list(entity);
    }

}