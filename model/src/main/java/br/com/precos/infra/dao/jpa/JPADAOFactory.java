package br.com.precos.infra.dao.jpa;

import br.com.precos.infra.dao.IDAO;
import br.com.precos.infra.dao.IDaoFactory;

import com.mysema.query.types.EntityPath;
import com.mysema.query.types.expr.SimpleExpression;

public class JPADAOFactory implements IDaoFactory {

    public static <E, I> IDAO<E, I> createDao(EntityPath<E> entity, SimpleExpression<I> idProperty) {
	return new JPAQueryDAO<E, I>(entity, idProperty);
    }

}
