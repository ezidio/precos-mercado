package br.com.precos.infra.repository;

import java.util.List;

import com.google.common.base.Optional;
import com.mysema.query.types.EntityPath;
import com.mysema.query.types.Expression;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.expr.SimpleExpression;

import br.com.precos.infra.dao.IDAO;
import br.com.precos.infra.dao.jpa.JPADAOFactory;
import br.com.precos.service.ListResult;

public class AbstractRepository<E, I> {

    IDAO<E, I> dao;
    
    public AbstractRepository(EntityPath<E> entity, SimpleExpression<I> idProperty) {
	this.dao = JPADAOFactory.createDao(entity, idProperty);
    }

    public <T> T findById(Expression<T> projection, I id) {
	return dao.findById(projection, id);
    }

    public E findById(I id) {
	return dao.findById(id);
    }

    public <T> ListResult<T> search(Expression<T> projection, Optional<BooleanExpression> expr, long start, long pageSize) {
	return dao.search(projection, expr, start, pageSize);
    }

    public ListResult<E> search(Optional<BooleanExpression> expr, long start, long pageSize) {
	return dao.search(expr, start, pageSize);
    }

    public <T> List<T> list(Expression<T> projection, Optional<BooleanExpression> expr) {
	return dao.list(projection, expr);
    }

    public List<E> list(Optional<BooleanExpression> expr) {
	return dao.list(expr);
    }

    public <T> List<T> list(Expression<T> projection) {
	return dao.list(projection);
    }

    public List<E> list() {
	return dao.list();
    }

}
