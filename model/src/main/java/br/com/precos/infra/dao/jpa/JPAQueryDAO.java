package br.com.precos.infra.dao.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.precos.infra.dao.AbstractDAO;
import br.com.precos.infra.dao.IDAO;
import br.com.precos.service.ListResult;

import com.google.common.base.Optional;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.EntityPath;
import com.mysema.query.types.Expression;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.expr.SimpleExpression;

public class JPAQueryDAO<E, I> extends AbstractDAO<E, I> implements IDAO<E, I> {

    @PersistenceContext
    private EntityManager em;

    protected JPAQueryDAO(EntityPath<E> entity, SimpleExpression<I> idProperty) {
	this.entity = entity;
	this.idProperty = idProperty;
    }

    protected JPAQuery createQuery(Optional<BooleanExpression> expr) {
	return new JPAQuery(em).from(this.entity);
    }

    protected JPAQuery createQuery(BooleanExpression expr) {
	return createQuery(Optional.fromNullable(expr));
    }

    /*
     * (non-Javadoc)
     * 
     * @see br.com.precos.infra.model.IDAO#findById(com.mysema.query.types.Expression , I)
     */
    @Override
    public <T> T findById(Expression<T> projection, I id) {
	return createQuery(idProperty.eq(id)).uniqueResult(projection);
    }

    /*
     * (non-Javadoc)
     * 
     * @see br.com.precos.infra.model.IDAO#search(com.mysema.query.types.Expression, com.google.common.base.Optional, long, long)
     */
    @Override
    public <T> ListResult<T> search(Expression<T> projection, Optional<BooleanExpression> expr, long start, long pageSize) {

	JPAQuery query = createQuery(expr);

	long count = query.count();

	if (pageSize > 0) {
	    query.offset(start).limit(pageSize);
	}

	List<T> values = query.list(projection);

	return new ListResult<>(start, pageSize, count, values);
    }

    /*
     * (non-Javadoc)
     * 
     * @see br.com.precos.infra.model.IDAO#list(com.mysema.query.types.Expression, com.google.common.base.Optional)
     */
    @Override
    public <T> List<T> list(Expression<T> projection, Optional<BooleanExpression> expr) {

	JPAQuery query = createQuery(expr);

	if (expr.isPresent()) {
	    query.where(expr.get());
	}

	return query.list(projection);
    }
}
