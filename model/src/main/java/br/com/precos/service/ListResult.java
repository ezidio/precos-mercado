package br.com.precos.service;

import java.util.List;

public class ListResult<T> {

	private final long pageSize;
	private final long count;
	private final long start;
	private final List<T> values;

	public ListResult(long pageSize, long count, long start, List<T> values) {
		super();
		this.pageSize = pageSize;
		this.count = count;
		this.start = start;
		this.values = values;
	}

	public long getPageSize() {
		return pageSize;
	}

	public long getCount() {
		return count;
	}

	public long getStart() {
		return start;
	}

	public List<T> getValues() {
		return values;
	}

}
