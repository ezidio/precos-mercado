package br.com.precos.domain.localization;

import org.springframework.stereotype.Repository;

import com.google.common.base.Optional;
import com.mysema.query.types.Expression;

import br.com.precos.infra.repository.AbstractRepository;

@Repository
public class CityQueryRepository extends AbstractRepository<City, Long> {

    public CityQueryRepository() {
	super(QCity.city, QCity.city.id);
    }

    public <T> void byState(Expression<T> projection, State state) {
	this.list(projection, Optional.of(QCity.city.state.eq(state)));
    }
}