package br.com.precos.domain.place;

import org.springframework.stereotype.Repository;

import br.com.precos.infra.repository.AbstractRepository;

@Repository
public class PlaceQueryRepository extends AbstractRepository<Place, Long> {

    public PlaceQueryRepository() {
	super(QPlace.place, QPlace.place.id);
    }
}