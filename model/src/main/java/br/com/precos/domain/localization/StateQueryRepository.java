package br.com.precos.domain.localization;

import org.springframework.stereotype.Repository;

import br.com.precos.infra.repository.AbstractRepository;

@Repository
public class StateQueryRepository extends AbstractRepository<State, Long> {

    public StateQueryRepository() {
	super(QState.state, QState.state.id);
    }
}