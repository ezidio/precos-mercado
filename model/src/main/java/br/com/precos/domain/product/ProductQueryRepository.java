package br.com.precos.domain.product;

import org.springframework.stereotype.Repository;

import br.com.precos.infra.repository.AbstractRepository;

@Repository
public class ProductQueryRepository extends AbstractRepository<Product, Long> {

    public ProductQueryRepository() {
	super(QProduct.product, QProduct.product.id);
    }
}
