define(['angular', 'gumga/components/pagination' ], function(angular) {
	'use strict';
	
	return angular

	  .module('app.controllers.pagination', ['gumga.components.pagination'])


	  .controller('GumgaPaginationController', ['$scope', '$location', '$http', function ($scope, $location, $http) {

			$scope.dadosPaginacao = {
				'count' : 90,
				'start' : 40,
				'pageSize' : 10
			}

		
	  }]);


})