
define(['angular', 'bootstrap','gumga/components/autocomplete'], function(angular, $) {

    var app = angular.module('app.controllers.autocomplete', ['gumga.components.autocomplete']);

    app.controller("GumgaAutocompleteCtrl", function($scope) {

        var characterList = [
            {id : 1, name: 'Chaves' },
            {id : 2, name: 'Chiquinha' },
            {id : 3, name: 'Seu Barriga' },
            {id : 4, name: 'Dona Florinda' },
            {id : 5, name: 'Professor Girafales' },
            {id : 6, name: 'Kiko' },
            {id : 7, name: 'Seu Madruga' },
            {id : 8, name: 'Bruxa do 71' },
            {id : 9, name: 'Nhonho' },
            {id : 10, name: 'Jaiminho' }
        ];

        var chavesSource = new Bloodhound({
          datumTokenizer: function(d) { return Bloodhound.tokenizers.whitespace(d.name); },
          queryTokenizer: Bloodhound.tokenizers.whitespace,
          local: characterList
        });
         
        // initialize the bloodhound suggestion engine
        chavesSource.initialize();

        $scope.config =  {
            displayKey : 'name',
            source : chavesSource.ttAdapter(),
            templates : {
                'suggestion' : Handlebars.compile('({{id}}) {{name}}')
            }
        }

    });

    app.controller("GumgaArrayAutocompleteCtrl", function($scope) {

        $scope.characterList = [
            {id : 1, name: 'Chaves' },
            {id : 2, name: 'Chiquinha' },
            {id : 3, name: 'Seu Barriga' },
            {id : 4, name: 'Dona Florinda' },
            {id : 5, name: 'Professor Girafales' },
            {id : 6, name: 'Kiko' },
            {id : 7, name: 'Seu Madruga' },
            {id : 8, name: 'Bruxa do 71' },
            {id : 9, name: 'Nhonho' },
            {id : 10, name: 'Jaiminho' }
        ];

    });

    app.controller("GumgaRemoteAutocompleteCtrl", function($scope) {
        $scope.baseUrl = "http://localhost:9080/gumga/";
        $scope.remoteUrl = $scope.baseUrl + "personagens/chaves";
    });

});