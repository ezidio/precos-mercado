define(['angular', 'gumga/components/table' ], function(angular) {
	'use strict';
	
	return angular

	  .module('app.controllers.table', ['gumga.components.table'])


	  .controller('GumgaTableCtrl', ['$scope', '$location', '$http', function ($scope, $location, $http) {

			$scope.selection = [];

			$scope.columnConfiguration = [
				{"label" : "Personagem", "field" : "name"},
				{"label" : "Gênero", "field" : "gender"}
			];

	        $scope.characterList = {'values' : [
	            {id : 1, name: 'Chaves', gender : 'M' },
	            {id : 2, name: 'Chiquinha', gender : 'F' },
	            {id : 3, name: 'Seu Barriga', gender : 'M' },
	            {id : 4, name: 'Dona Florinda', gender : 'F' },
	            {id : 5, name: 'Professor Girafales', gender : 'M' },
	            {id : 6, name: 'Kiko', gender : 'M' },
	            {id : 7, name: 'Seu Madruga', gender : 'M' },
	            {id : 8, name: 'Bruxa do 71', gender : 'F' },
	            {id : 9, name: 'Nhonho', gender : 'M' },
	            {id : 10, name: 'Jaiminho', gender : 'M' }
	        ]};


		
	  }]);


})