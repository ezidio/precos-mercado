package br.com.precos.infrastructure.ormlite.repository;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.SQLException;

import br.com.precos.domain.localization.City;
import br.com.precos.domain.localization.State;
import br.com.precos.domain.place.Place;
import br.com.precos.domain.product.Product;
import br.com.precos.domain.shop.ShopItem;
import br.com.precos.domain.shop.ShopList;
import br.com.precos.domain.user.User;
import br.com.precos.infrastructure.ormlite.converter.AmountType;
import br.com.precos.infrastructure.ormlite.converter.EAN13Type;
import br.com.precos.infrastructure.ormlite.converter.EmailType;
import br.com.precos.infrastructure.ormlite.converter.MoneyType;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.field.DataPersisterManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.support.DatabaseConnection;
import com.j256.ormlite.table.TableUtils;

public class ConnectionManager {

    private static final Class<?>[] entities = new Class[] {
	State.class,
	City.class,
	Place.class,
	Product.class,
	User.class,
	ShopList.class,
	ShopItem.class
    };
    
    private static JdbcConnectionSource connectionSource;

    public static ConnectionSource getConnection() throws SQLException {
	if (ConnectionManager.connectionSource == null) {
	    return createConnection("/data.sql");
	} else {
	    return ConnectionManager.connectionSource;
	}
    }
    
    public static <T, ID> Dao<T, ID> createDAO(Class<T> entity) throws SQLException {
        return DaoManager.createDao(ConnectionManager.getConnection(), entity);
    }

    private static JdbcConnectionSource createConnection(String dataFile) throws SQLException {
	DataPersisterManager.registerDataPersisters(EAN13Type.getSingleton());
	DataPersisterManager.registerDataPersisters(EmailType.getSingleton());
	DataPersisterManager.registerDataPersisters(AmountType.getSingleton());
	DataPersisterManager.registerDataPersisters(MoneyType.getSingleton());
	
	ConnectionManager.connectionSource = new JdbcConnectionSource("jdbc:h2:mem:account");
	
	for (Class<?> entity : entities) {
	    TableUtils.createTable(connectionSource, entity);
	}
	
	executeSQLFile(dataFile);
	
	return ConnectionManager.connectionSource;
    }
    
    private static void executeSQLFile(String dataFile) {
	
	try {
            InputStream is = StateIT.class.getResourceAsStream(dataFile);
            DataInputStream in = new DataInputStream(is);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String strLine;
            while ((strLine = br.readLine()) != null) {
        	connectionSource.getReadWriteConnection().executeStatement(strLine, DatabaseConnection.DEFAULT_RESULT_FLAGS);
            }
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}
