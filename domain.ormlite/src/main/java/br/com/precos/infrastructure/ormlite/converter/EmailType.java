package br.com.precos.infrastructure.ormlite.converter;

import java.sql.SQLException;

import br.com.precos.domain.type.Email;

import com.j256.ormlite.field.FieldType;
import com.j256.ormlite.field.SqlType;
import com.j256.ormlite.field.types.StringType;

public class EmailType extends StringType {

    private static final EmailType singleTon = new EmailType();

    public static EmailType getSingleton() {
	return singleTon;
    }

    protected EmailType() {
	super(SqlType.STRING, new Class[] { Email.class });
    }

    @Override
    public Object javaToSqlArg(FieldType fieldType, Object javaObject) throws SQLException {
	Email code = Email.class.cast(javaObject);
	return super.javaToSqlArg(fieldType, code.toString());
    }

    @Override
    public Object sqlArgToJava(FieldType fieldType, Object sqlArg, int columnPos) throws SQLException {
	Object value = super.sqlArgToJava(fieldType, sqlArg, columnPos);
	return new Email(String.class.cast(value));
    }

}
