package br.com.precos.infrastructure.ormlite.repository;

import br.com.precos.domain.place.IPlaceRepository;
import br.com.precos.domain.place.Place;

public class ORMLitePlaceRepository extends ORMLiteBaseQueryRepository<Place, Long> implements IPlaceRepository {

    public ORMLitePlaceRepository() {
	super(Place.class);
    }

}
