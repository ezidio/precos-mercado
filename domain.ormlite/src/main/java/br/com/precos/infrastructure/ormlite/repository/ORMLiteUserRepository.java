package br.com.precos.infrastructure.ormlite.repository;

import br.com.precos.domain.user.IUserRepository;
import br.com.precos.domain.user.User;

public class ORMLiteUserRepository extends ORMLiteBaseQueryRepository<User, Long> implements IUserRepository {

    public ORMLiteUserRepository() {
	super(User.class);
    }

}
