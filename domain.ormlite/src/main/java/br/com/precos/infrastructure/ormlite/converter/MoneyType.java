package br.com.precos.infrastructure.ormlite.converter;

import java.math.BigDecimal;
import java.sql.SQLException;

import br.com.precos.domain.type.Money;

import com.j256.ormlite.field.FieldType;
import com.j256.ormlite.field.SqlType;
import com.j256.ormlite.field.types.BigDecimalNumericType;

public class MoneyType extends BigDecimalNumericType {

    private static final MoneyType singleTon = new MoneyType();

    public static MoneyType getSingleton() {
	return singleTon;
    }
    
    protected MoneyType() {
	super(SqlType.BIG_DECIMAL, new Class[] {Money.class});
    }

    @Override
    public Object parseDefaultString(FieldType fieldType, String defaultStr) throws SQLException {
	return super.parseDefaultString(fieldType, defaultStr);
    }

    @Override
    public Object javaToSqlArg(FieldType fieldType, Object javaObject) throws SQLException {
	Money money = Money.class.cast(javaObject);
	return super.javaToSqlArg(fieldType, money.toBigDecimal());
    }

    @Override
    public Object sqlArgToJava(FieldType fieldType, Object sqlArg, int columnPos) throws SQLException {
	Object bigDecimalValue = super.sqlArgToJava(fieldType, sqlArg, columnPos);
	return new Money(BigDecimal.class.cast(bigDecimalValue));
    }
    
    

}
