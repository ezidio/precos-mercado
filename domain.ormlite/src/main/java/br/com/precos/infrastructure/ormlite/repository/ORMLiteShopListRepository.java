package br.com.precos.infrastructure.ormlite.repository;

import java.sql.SQLException;
import java.util.Collection;

import br.com.precos.domain.shop.IShopListRepository;
import br.com.precos.domain.shop.ShopItem;
import br.com.precos.domain.shop.ShopList;
import br.com.precos.infrastructure.repository.exception.RepositoryException;

import com.j256.ormlite.dao.Dao;

public class ORMLiteShopListRepository extends ORMLiteBaseQueryRepository<ShopList, Long> implements IShopListRepository {

    public ORMLiteShopListRepository() {
	super(ShopList.class);
    }

    public void save(ShopList list) {
	try {
	    Dao<ShopList, Long> listDao = ConnectionManager.createDAO(ShopList.class);
	    Dao<ShopItem, Long> itemDao = ConnectionManager.createDAO(ShopItem.class);

	    listDao.create(list);

	    Collection<ShopItem> items = list.getItems();
	    for (ShopItem item : items) {
		itemDao.create(item);
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	    throw new RepositoryException(e);
	}

    }


}
