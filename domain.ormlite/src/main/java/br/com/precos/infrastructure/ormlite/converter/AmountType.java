package br.com.precos.infrastructure.ormlite.converter;

import java.math.BigDecimal;
import java.sql.SQLException;

import br.com.precos.domain.type.Amount;

import com.j256.ormlite.field.FieldType;
import com.j256.ormlite.field.SqlType;
import com.j256.ormlite.field.types.BigDecimalNumericType;

public class AmountType extends BigDecimalNumericType {

    private static final AmountType singleTon = new AmountType();

    public static AmountType getSingleton() {
	return singleTon;
    }
    
    protected AmountType() {
	super(SqlType.BIG_DECIMAL, new Class[] {Amount.class});
    }

    @Override
    public Object javaToSqlArg(FieldType fieldType, Object javaObject) throws SQLException {
	Amount amount = Amount.class.cast(javaObject);
	return super.javaToSqlArg(fieldType, amount.toBigDecimal());
    }

    @Override
    public Object sqlArgToJava(FieldType fieldType, Object sqlArg, int columnPos) throws SQLException {
	Object bigDecimalValue = super.sqlArgToJava(fieldType, sqlArg, columnPos);
	return new Amount(BigDecimal.class.cast(bigDecimalValue));
    }

}
