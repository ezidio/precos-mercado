package br.com.precos.infrastructure.ormlite.repository;

import java.sql.SQLException;
import java.util.List;

import br.com.precos.infrastructure.repository.IQueryRepository;
import br.com.precos.infrastructure.repository.exception.RepositoryException;

import com.google.common.base.Optional;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;

public class ORMLiteBaseQueryRepository<T, ID> implements IQueryRepository<T, ID> {

    private Class<T> type;

    public ORMLiteBaseQueryRepository(Class<T> type) {
	this.type = type;
    }

    protected Dao<T, ID> getDAO() {
	try {
	    return ConnectionManager.createDAO(this.type);
	} catch (SQLException e) {
	    throw new RepositoryException("Problema ao recuperar o DAO", e);
	}
    }
    
    protected QueryBuilder<T, ID> queryBuilder() {
	return getDAO().queryBuilder();
    }

    @Override
    public Optional<T> getById(ID id) {
	try {
	    return Optional.fromNullable(getDAO().queryForId(id));
	} catch (SQLException e) {
	    throw new RepositoryException(e);
	}
    }

    @Override
    public List<T> getAll() {
	try {
	    return getDAO().queryForAll();
	} catch (SQLException e) {
	    throw new RepositoryException(e);
	}
    }

}