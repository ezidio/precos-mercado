package br.com.precos.infrastructure.ormlite.converter;

import java.sql.SQLException;

import br.com.precos.domain.type.barcode.CustomEAN13;
import br.com.precos.domain.type.barcode.EAN13;

import com.j256.ormlite.field.FieldType;
import com.j256.ormlite.field.SqlType;
import com.j256.ormlite.field.types.StringType;

public class EAN13Type extends StringType {

    private static final EAN13Type singleTon = new EAN13Type();

    public static EAN13Type getSingleton() {
	return singleTon;
    }

    protected EAN13Type() {
	super(SqlType.STRING, new Class[] { EAN13.class, CustomEAN13.class, CustomEAN13.class });
    }

    @Override
    public Object javaToSqlArg(FieldType fieldType, Object javaObject) throws SQLException {
	EAN13 code = EAN13.class.cast(javaObject);
	return super.javaToSqlArg(fieldType, code.toString());
    }

    @Override
    public Object sqlArgToJava(FieldType fieldType, Object sqlArg, int columnPos) throws SQLException {
	Object value = super.sqlArgToJava(fieldType, sqlArg, columnPos);
	return EAN13.from(String.class.cast(value));
    }

}
