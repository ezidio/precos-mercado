package br.com.precos.infrastructure.ormlite.repository;

import br.com.precos.domain.product.IProductRepository;
import br.com.precos.domain.product.Product;

public class ORMLiteProductRepository extends ORMLiteBaseQueryRepository<Product, Long> implements IProductRepository {

    public ORMLiteProductRepository() {
	super(Product.class);
    }

}
