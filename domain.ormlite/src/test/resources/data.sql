INSERT INTO state (id, name, abbreviation) values (1, 'Acre', 'AC');
INSERT INTO state (id, name, abbreviation) values (2, 'Alagoas', 'AL');
INSERT INTO state (id, name, abbreviation) values (3, 'Amazonas', 'AM');
INSERT INTO state (id, name, abbreviation) values (4, 'Amapá', 'AP');
INSERT INTO state (id, name, abbreviation) values (5, 'Bahia', 'BA');
INSERT INTO state (id, name, abbreviation) values (6, 'Ceará', 'CE');
INSERT INTO state (id, name, abbreviation) values (7, 'Distrito Federal', 'DF');
INSERT INTO state (id, name, abbreviation) values (8, 'Espírito Santo', 'ES');
INSERT INTO state (id, name, abbreviation) values (9, 'Goiás', 'GO');
INSERT INTO state (id, name, abbreviation) values (10, 'Maranhão', 'MA');
INSERT INTO state (id, name, abbreviation) values (11, 'Minas Gerais', 'MG');
INSERT INTO state (id, name, abbreviation) values (12, 'Mato Grosso do Sul', 'MS');
INSERT INTO state (id, name, abbreviation) values (13, 'Mato Grosso', 'MT');
INSERT INTO state (id, name, abbreviation) values (14, 'Pará', 'PA');
INSERT INTO state (id, name, abbreviation) values (15, 'Paraíba', 'PB');
INSERT INTO state (id, name, abbreviation) values (16, 'Pernambuco', 'PE');
INSERT INTO state (id, name, abbreviation) values (17, 'Piauí', 'PI');
INSERT INTO state (id, name, abbreviation) values (18, 'Paraná', 'PR');
INSERT INTO state (id, name, abbreviation) values (19, 'Rio de Janeiro', 'RJ');
INSERT INTO state (id, name, abbreviation) values (20, 'Rio Grande do Norte', 'RN');
INSERT INTO state (id, name, abbreviation) values (21, 'Rondônia', 'RO');
INSERT INTO state (id, name, abbreviation) values (22, 'Roraima', 'RR');
INSERT INTO state (id, name, abbreviation) values (23, 'Rio Grande do Sul', 'RS');
INSERT INTO state (id, name, abbreviation) values (24, 'Santa Catarina', 'SC');
INSERT INTO state (id, name, abbreviation) values (25, 'Sergipe', 'SE');
INSERT INTO state (id, name, abbreviation) values (26, 'São Paulo', 'SP');
INSERT INTO state (id, name, abbreviation) values (27, 'Tocantins', 'TO');

INSERT INTO city (id, name, id_state) values (1, 'Maringá', 18);

INSERT INTO place (id, description, id_city) values (1, 'Geral', 1);
INSERT INTO place (id, description, id_city) values (2, 'Cidade Canção', 1);

INSERT INTO product (id, description, barcode, id_place) values (1, 'Fosforo Fiat Lux', '7896007941636', 1);

INSERT INTO user (id, name, email) values (1, 'Carmerino Tanaka', 'carmerino@email.com');

INSERT INTO shoplist (id, id_place, date, id_user, status) values (1, 1, 1399143376398, 1, 'DRAFT');

INSERT INTO shopitem (id_list, id_product, price, amount) values (1, 1, 1.45, 2);
