package br.com.precos.infrastructure.ormlite.repository;

import static org.junit.Assert.assertEquals;

import java.sql.SQLException;
import java.util.List;

import org.junit.Test;

import br.com.precos.domain.localization.State;

public class StateIT {

    @Test
    public void testQueryForAll() throws SQLException {
	List<State> states = ConnectionManager.createDAO(State.class).queryForAll();
	assertEquals(27, states.size());
    }

    @Test
    public void testQueryById() throws SQLException {
	State acre = ConnectionManager.createDAO(State.class).queryForId(Long.valueOf(1));
	assertEquals(Long.valueOf(1), acre.getId());
	assertEquals("AC", acre.getAbbreviation());
	assertEquals("Acre", acre.getName());
    }

}
