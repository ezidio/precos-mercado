package br.com.precos.infrastructure.ormlite.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.sql.SQLException;
import java.util.List;

import org.joda.time.DateTime;
import org.junit.Test;

import br.com.precos.domain.place.Place;
import br.com.precos.domain.product.Product;
import br.com.precos.domain.shop.ShopItem;
import br.com.precos.domain.shop.ShopList;
import br.com.precos.domain.type.Amount;
import br.com.precos.domain.type.Money;
import br.com.precos.domain.user.User;
import br.com.precos.infrastructure.ormlite.repository.ORMLitePlaceRepository;
import br.com.precos.infrastructure.ormlite.repository.ORMLiteProductRepository;
import br.com.precos.infrastructure.ormlite.repository.ORMLiteUserRepository;

import com.google.common.base.Optional;

public class ShopListIT {

    @Test
    public void testQueryById() throws SQLException {
	System.out.println(DateTime.now().getMillis());
	
	ShopList list = ConnectionManager.createDAO(ShopList.class).queryForId(Long.valueOf(1));
	assertEquals(Long.valueOf(1), list.getId());
	assertEquals(1, list.getItems().size());
    }
    
    @Test
    public void testInsertList() {
	ORMLiteShopListRepository shopListRepository = new ORMLiteShopListRepository();
	
	Optional<User> user = new ORMLiteUserRepository().getById(Long.valueOf(1L));
	Optional<Place> place = new ORMLitePlaceRepository().getById(Long.valueOf(1L));
	Optional<Product> product = new ORMLiteProductRepository().getById(Long.valueOf(1L));
	
	assertTrue(user.isPresent());
	assertTrue(place.isPresent());
	assertTrue(product.isPresent());
	
	ShopList list = new ShopList(user.get(), place.get());
	list.addItem(new ShopItem(product.get(), Money.valueOf(1.5), Amount.valueOf(2)));
	
	shopListRepository.save(list);
	
	assertNotNull(list.getId());
	
	List<ShopList> lists = shopListRepository.getAll();
	assertEquals(2, lists.size());
    }

}
