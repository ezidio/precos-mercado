package br.com.precos.infrastructure.ormlite.repository;

import java.sql.SQLException;

import org.junit.Test;

import br.com.precos.domain.localization.City;
import br.com.precos.domain.localization.State;

import com.j256.ormlite.dao.Dao;


public class CityIT {

    @Test
    public void should_insert_a_city() throws SQLException {
	
	Dao<State, Long> stateDao = ConnectionManager.createDAO(State.class);
	
	Dao<City, Long> dao = ConnectionManager.createDAO(City.class);
	
	
	City newCity = new City("Maringá", stateDao.queryBuilder().where().eq("abbreviation", "PR").queryForFirst());
	dao.create(newCity);
	
    }

}
