package br.com.precos.infrastructure.ormlite.repository;

import static org.junit.Assert.assertEquals;

import java.sql.SQLException;

import org.joda.time.DateTime;
import org.junit.Test;

import br.com.precos.domain.user.User;

public class UserIT {

    @Test
    public void testQueryById() throws SQLException {
	System.out.println(DateTime.now().getMillis());
	
	User usuario = ConnectionManager.createDAO(User.class).queryForId(Long.valueOf(1));
	assertEquals(Long.valueOf(1), usuario.getId());
	assertEquals("Carmerino Tanaka", usuario.getName());
	assertEquals("carmerino@email.com", usuario.getEmail().toString());
    }

}
