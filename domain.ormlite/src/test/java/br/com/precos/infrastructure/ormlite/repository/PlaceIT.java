package br.com.precos.infrastructure.ormlite.repository;

import java.sql.SQLException;

import org.junit.Test;

import br.com.precos.domain.localization.City;
import br.com.precos.domain.place.Place;

import com.j256.ormlite.dao.Dao;

public class PlaceIT {
    
    @Test
    public void should_insert_a_place() throws SQLException {
	
	Dao<City, Long> cityDao = ConnectionManager.createDAO(City.class);
	City maringa = cityDao.queryForId(1L);
	
	Dao<Place, Long> dao = ConnectionManager.createDAO(Place.class);
	
	Place place = new Place("Cidade Canção", maringa);
	
	dao.create(place);
	
	Place searchPlace = dao.queryBuilder().where().eq("description", "Cidade Canção").queryForFirst();
	System.out.println(searchPlace);
	System.out.println(searchPlace.getCity().getName());
	
	
    }
}
