package br.com.precos.infrastructure.ormlite.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.sql.SQLException;

import org.junit.Test;

import br.com.precos.domain.product.Product;
import br.com.precos.domain.type.barcode.DefaultEAN13;
import br.com.precos.domain.type.barcode.EAN13;

import com.j256.ormlite.dao.Dao;

public class ProductIT {
    
    @Test
    public void should_retrieve_the_product() throws SQLException {
	Dao<Product, Long> productDAO = ConnectionManager.createDAO(Product.class);
	Product product = productDAO.queryForId(1L);
	assertNotNull(product);
	assertEquals(Long.valueOf(1), product.getId());
	assertEquals("Fosforo Fiat Lux", product.getDescription());
	assertEquals(EAN13.from("7896007941636"), product.getBarcode());
	assertTrue(DefaultEAN13.class.isInstance(product.getBarcode()));
	
    }
}
