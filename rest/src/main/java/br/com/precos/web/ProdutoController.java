package br.com.precos.web;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.precos.domain.place.Place;
import br.com.precos.domain.place.PlaceQueryRepository;
import br.com.precos.service.QueryObject;

@RestController
public class ProdutoController {

	@Autowired
	private PlaceQueryRepository repository;
	
	@RequestMapping("/products" )
	public List<Place> list(QueryObject request) {
		return this.repository.list();
	}
	
	

}
