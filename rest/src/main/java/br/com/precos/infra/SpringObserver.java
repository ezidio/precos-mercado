package br.com.precos.infra;

import java.util.concurrent.atomic.AtomicBoolean;

public class SpringObserver {
	
	private static AtomicBoolean SPRING_READY = new AtomicBoolean();
	
	public static void releaseSpring() {
		SPRING_READY.getAndSet(true);
	}
	
	public static boolean isSpringReady() {
		return SPRING_READY.get();
	}

}
