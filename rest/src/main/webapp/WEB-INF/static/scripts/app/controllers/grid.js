define(['angular', 'jquery-blockui', 'gumga/components/grid' ], function(angular) {
	'use strict';
	
	return angular

	  .module('app.controllers.grid', ['gumga.components.grid'])


	  .controller('GumgaGridCtrl', ['$scope', '$location', '$http', function ($scope, $location, $http) {

		$scope.gridSelection = [];

		$scope.gridColumns = [
			{"label" : "Player", "field" : "name"},
			{"label" : "Team", "field" : "team"}
		];


		
	  }]);


})