package br.com.precos.crawler;

import com.google.common.base.Objects;

public class Product {

    private String title;
    private String code;
    private String image;


    public Product(String title, String code) {
	this.title = title;
	this.code = code;
    }

    public String getTitle() {
	return title;
    }

    public String getCode() {
	return code;
    }

    @Override
    public int hashCode() {
	return code.hashCode();
    }

    @Override
    public String toString() {
	return Objects.toStringHelper(this).add("code", code).add("title", title).add("image", image).toString();
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

 
}
