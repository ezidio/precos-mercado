package br.com.precos.crawler;

import java.util.Set;

import org.jsoup.nodes.Document;

import com.google.common.base.Optional;

public interface ProductSite {

    String getStartPage();

    Optional<Product> getProductInfo(Document dom);

    Set<String> getLinks(Document dom, Set<String> urls);

}