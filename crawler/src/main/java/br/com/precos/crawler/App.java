package br.com.precos.crawler;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Stack;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import com.google.common.base.Optional;

public class App {

    Set<String> pageSet = new HashSet<String>();
    Stack<String> pages = new Stack<String>();
    int pointer = 0;

    List<ProductSite> sites = new ArrayList<ProductSite>();

    /**
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
	new App().start();
    }

    private void start() throws IOException {
	sites.add(new MercadoRibeirao());

	FileWriter writer = null;
	try {
	    writer = new FileWriter(new File("out_online.txt"));
	    for (ProductSite site : this.sites) {
		retrieveSiteData(writer, site);
	    }
	} finally {
	    writer.close();
	}
    }

    private void retrieveSiteData(FileWriter writer, ProductSite site) {
	pages.add(site.getStartPage());

	String pageURL;
	
	while (!pages.empty()) {
	    try {
		
		pageURL = pages.pop();
		
		Document dom = Jsoup.connect(pageURL).get();
		Set<String> links = site.getLinks(dom, this.pageSet);
		if (links.size() > 0) {
		    System.out.println("adicionado "+links.size()+" links na pilha");
		    pageSet.addAll(links);
		    pages.addAll(links);
		}
		
		Optional<Product> productOpt = site.getProductInfo(dom);

		if (productOpt.isPresent()) {
		    Product product = productOpt.get();

		    writer.write(product.getCode());
		    writer.write(';');
		    writer.write(product.getTitle());
		    writer.write(';');
		    writer.write(product.getImage() == null ? "" : product.getImage());
		    writer.write("\n");
		    writer.flush();
		    System.out.println(product);
		}

	    } catch (IOException e) {
		e.printStackTrace();
	    }
	}
    }



}
