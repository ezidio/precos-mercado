package br.com.precos.crawler;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.google.common.base.Optional;

public class OnlineSupermercado implements ProductSite {

    
    /* (non-Javadoc)
     * @see br.com.precos.crawler.ProductSite#getStartPage()
     */
    public String getStartPage() {
	return "http://www.onlinesupermercado.com.br/caldo-galinha-maggi-63g-p4580";
    }
    
    /* (non-Javadoc)
     * @see br.com.precos.crawler.ProductSite#getLinkElements(org.jsoup.nodes.Document)
     */
    public List<Element> getLinkElements(Document dom) {
	List<Element> elements = new ArrayList<Element>();
	elements.addAll(dom.select(".item_box_produto a"));
	elements.addAll(dom.select(".subitem_menu_categorias_vertical a"));
	elements.addAll(dom.select("a.numero_pagina"));
	return elements;
    }
    
    
    public Set<String> getLinks(Document dom, Set<String> urls) {
	
   	Set<String> links = new HashSet<String>();
   	
   	List<Element> elements = new ArrayList<Element>();
	elements.addAll(dom.select(".item_box_produto a"));
	elements.addAll(dom.select(".subitem_menu_categorias_vertical a"));
	elements.addAll(dom.select("a.numero_pagina"));
   	
   	for (Element element : elements) {
	    try {
		String href = element.attr("href");
		new URL(href);
		if (!urls.contains(href)) {
		    links.add(href);
		}
	    } catch (MalformedURLException e) {
		// ignora a URL
	    }
   	}
   	
   	return links;
       }
    
    /* (non-Javadoc)
     * @see br.com.precos.crawler.ProductSite#getProductInfo(org.jsoup.nodes.Document)
     */
    public Optional<Product> getProductInfo(Document dom) {
	Elements title = dom.select(".texto_cabecalho_pagina");
	Elements barcode = dom.select("#produto_cod_ref");
	if (title.size() > 0 && barcode.size() > 0) {
	    return Optional.of(new Product(title.get(0).text(), barcode.get(0).text()));
	} else {
	    return Optional.absent();
	}
    }

}
