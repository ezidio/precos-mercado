package br.com.precos.crawler;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.google.common.base.Optional;

public class Roge implements ProductSite {

    
    private static final String BASE_URL = "http://www.roge.com.br/";

    /* (non-Javadoc)
     * @see br.com.precos.crawler.ProductSite#getStartPage()
     */
    public String getStartPage() {
	return "http://www.roge.com.br/produto-detalhe.aspx?id=243395";
    }
    
    public Set<String> getLinks(Document dom, Set<String> urls) {
	
	Set<String> links = new HashSet<String>();
	
	List<Element> elements = new ArrayList<Element>();
	elements.addAll(dom.select("td.dvListaDescricao a"));
	elements.addAll(dom.select("ul.navsuperior a"));
	elements.addAll(dom.select("#MainContent_pnlCategorias a"));
	elements.addAll(dom.select("ul.paginacao a"));
	
	for (Element element : elements) {
	    try {
		String href = element.attr("href");
		if (!href.startsWith("http://")) {
		    href = BASE_URL + href;
		}
		new URL(href);
		
		if (!urls.contains(href)) {
		    links.add(href);
		}
	    } catch (MalformedURLException e) {
		// ignora a URL
	    }
	}
	
	return links;
    }
    
    /* (non-Javadoc)
     * @see br.com.precos.crawler.ProductSite#getProductInfo(org.jsoup.nodes.Document)
     */
    public Optional<Product> getProductInfo(Document dom) {
	Elements title = dom.select("#MainContent_lblProduto");
	Elements barcode = dom.select("#MainContent_lblInformacoes strong");
	if (title.size() > 0 && barcode.size() > 0) {
	    return Optional.of(new Product(title.get(0).text(), barcode.last().text()));
	} else {
	    return Optional.absent();
	}
    }

}
