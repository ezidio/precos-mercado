package br.com.precos.crawler;

public class WebPage {

    private String url;

    public WebPage(String url) {
	this.url = url;
    }

    @Override
    public int hashCode() {
	return this.url.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
	if (obj == null) return false;
	if (!WebPage.class.isInstance(obj)) return false;
	return WebPage.class.cast(obj).url.equals(this.url);
    }

    public String getUrl() {
        return url;
    }

}
