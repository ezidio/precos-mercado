package br.com.precos.crawler;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.google.common.base.Optional;

public class MercadoRibeirao implements ProductSite {

    
    private static final String BASE_URL = "https://www.mercadoribeirao.com.br/";

    /* (non-Javadoc)
     * @see br.com.precos.crawler.ProductSite#getStartPage()
     */
    public String getStartPage() {
	return BASE_URL+"index.php";
    }
    
    public Set<String> getLinks(Document dom, Set<String> urls) {
	
	Set<String> links = new HashSet<String>();
	
	List<Element> elements = new ArrayList<Element>();
	elements.addAll(dom.select("#categoriaSlide a"));
	elements.addAll(dom.select(".produtc-home a.img-border"));
	elements.addAll(dom.select(".boxProductLine a.preloading-light"));
	elements.addAll(dom.select(".page-pagination a"));
	
	for (Element element : elements) {
	    try {
		String href = element.attr("href");
		if (!href.startsWith("http://") && !href.startsWith("https://")) {
		    href = BASE_URL + href;
		}
		new URL(href);
		
		if (!urls.contains(href)) {
		    links.add(href);
		}
	    } catch (MalformedURLException e) {
		// ignora a URL
	    }
	}
	
	return links;
    }
    
    /* (non-Javadoc)
     * @see br.com.precos.crawler.ProductSite#getProductInfo(org.jsoup.nodes.Document)
     */
    public Optional<Product> getProductInfo(Document dom) {
	Elements title = dom.select(".entry-heading h1");
	Elements barcode = dom.select(".entry-heading span.category");
	Elements image = dom.select(".entry-content .image-preview img");
	if (title.size() > 0 && barcode.size() > 0) {
	    Product product = new Product(title.get(0).text(), barcode.last().text());
	    if (image.size() > 0) {
		product.setImage(image.get(0).attr("src"));
	    }
	    return Optional.of(product);
	} else {
	    return Optional.absent();
	}
    }

}
